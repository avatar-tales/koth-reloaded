package fr.avatarreturns.domination.game;

import org.bukkit.Color;

public enum SquadColor {

    RED(Color.fromRGB(255, 0, 0)),
    YELLOW(Color.fromRGB(243, 255, 0)),
    BLUE(Color.fromRGB(0, 0, 255)),
    GREEN(Color.fromRGB(0, 255, 0)),
    WHITE(Color.fromRGB(255, 255, 255)),
    BLACK(Color.fromRGB(0, 0, 0));

    private Color color;

    SquadColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}
