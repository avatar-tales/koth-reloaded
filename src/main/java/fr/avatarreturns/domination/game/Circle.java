package fr.avatarreturns.domination.game;

import fr.avatarreturns.domination.utils.VectorUtils;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@SuppressWarnings({"unused"})
public class Circle {

    @NotNull
    private final String id;
    @NotNull
    private Location location;
    @NotNull
    private Location teleport;
    private double radius;
    @NotNull
    private Color color;
    private double depth;
    private boolean showHitBox;

    private transient Location[] points;
    private transient Set<int[]> edges;

    public Circle(final @NotNull String id, final @NotNull Location location, final @NotNull Location teleport, final double radius, final @NotNull Color color, final double depth, final boolean showHitBox) {
        this.id = id;
        this.location = location;
        this.teleport = teleport;
        this.radius = radius;
        this.color = color;
        this.depth = depth;
        this.showHitBox = showHitBox;
        this.points = this.getHitBoxPoints();
    }

    public void render() {
        final int particles = 16;
        final double twoPI = 2 * Math.PI;
        final Vector nv = this.getLocation().getDirection().normalize();
        final Vector ya = VectorUtils.perp(nv, new Vector(0, 1, 0)).normalize();
        final Vector xa = ya.getCrossProduct(nv).normalize();
        nv.multiply(-1);
        for (double theta = 0; theta < particles; theta += twoPI / particles) {
            double angle = twoPI * theta / particles + Math.random() % (twoPI / particles);
            double ax = Math.cos(angle) * radius;
            double az = Math.sin(angle) * radius;
            double xi = xa.getX() * ax + ya.getX() * az;
            double yi = xa.getY() * ax + ya.getY() * az;
            double zi = xa.getZ() * ax + ya.getZ() * az;
            final @NotNull Location location = this.getLocation().clone().add(new Vector(xi, yi, zi));
            Objects.requireNonNull(location.getWorld()).spawnParticle(Particle.REDSTONE, location, 1, new Particle.DustOptions(this.color, 1));
        }
        if (this.showHitBox) {
            if (this.points == null)
                this.points = this.getHitBoxPoints();
            int i = 0;
            for (final Location point : this.points) {
                Objects.requireNonNull(point.getWorld()).spawnParticle(Particle.REDSTONE, point, 1, new Particle.DustOptions((i == 0 ? Color.RED : ((i == 1 || i == 2 || i == 4) ? Color.ORANGE : Color.NAVY)), 1));
                i++;
            }

            for (int[] index : this.edges) {
                final Location point1 = this.points[index[0]];
                final Location point2 = this.points[index[1]];
                Objects.requireNonNull(point2.getWorld()).spawnParticle(Particle.REDSTONE, point2, 1, new Particle.DustOptions(Color.NAVY, 1));
                final Vector vector = VectorUtils.from(point2, point1);
                for (int j = 1; j <= 5; j++) {
                    final Location tempLoc = point2.clone().add(j * vector.getX() / 5, j * vector.getY() / 5, j * vector.getZ() / 5);
                    Objects.requireNonNull(tempLoc.getWorld()).spawnParticle(Particle.REDSTONE, tempLoc, 1, new Particle.DustOptions(Color.AQUA, 1));
                }
            }
        }
    }

    public Location[] getHitBoxPoints() {
        final Location[] points = new Location[8];
        final Vector nv = this.getLocation().getDirection().normalize();
        final Vector ya = VectorUtils.perp(nv, new Vector(0, 1, 0)).normalize();
        final Vector xa = ya.getCrossProduct(nv).normalize();
        nv.multiply(-this.depth);
        final Vector toMiddlePoint = new Vector(nv.getX() * this.depth, this.depth * nv.getY(), this.depth * nv.getZ());
        double xb = xa.getX() * radius + ya.getX() * radius;
        double yb = xa.getY() * radius + ya.getY() * radius;
        double zb = xa.getZ() * radius + ya.getZ() * radius;
        double xc = -xa.getX() * radius + ya.getX() * radius;
        double zc = -xa.getZ() * radius + ya.getZ() * radius;
        double xd = xa.getX() * radius - ya.getX() * radius;
        double yd = xa.getY() * radius - ya.getY() * radius;
        double zd = xa.getZ() * radius - ya.getZ() * radius;
        double xe = -xa.getX() * radius + -ya.getX() * radius;
        double ze = -xa.getZ() * radius + -ya.getZ() * radius;
        points[0] = this.location.clone().add(toMiddlePoint).add(xb, yb, zb);
        points[1] = this.location.clone().add(toMiddlePoint).add(xc, yb, zc);
        points[2] = this.location.clone().add(toMiddlePoint).add(xd, yd, zd);
        points[3] = this.location.clone().add(toMiddlePoint).add(xe, yd, ze);
        points[4] = this.location.clone().subtract(toMiddlePoint).add(xb, yb, zb);
        points[5] = this.location.clone().subtract(toMiddlePoint).add(xc, yb, zc);
        points[6] = this.location.clone().subtract(toMiddlePoint).add(xd, yd, zd);
        points[7] = this.location.clone().subtract(toMiddlePoint).add(xe, yd, ze);

        if (this.edges == null)
            this.edges = new HashSet<>();
        else
            this.edges.clear();
        this.edges.add(new int[]{0, 1});
        this.edges.add(new int[]{0, 2});
        this.edges.add(new int[]{0, 4});
        this.edges.add(new int[]{1, 5});
        this.edges.add(new int[]{1, 3});
        this.edges.add(new int[]{2, 6});
        this.edges.add(new int[]{2, 3});
        this.edges.add(new int[]{3, 7});
        this.edges.add(new int[]{4, 5});
        this.edges.add(new int[]{4, 6});
        this.edges.add(new int[]{5, 7});
        this.edges.add(new int[]{6, 7});


        return points;
    }

    public boolean isLocationInCircle(final Location location) {
        if (!Objects.equals(this.location.getWorld(), location.getWorld()))
            return false;
        if (this.points == null)
            this.points = this.getHitBoxPoints();
        final Vector vectorI = VectorUtils.from(this.points[0], this.points[1]);
        final Vector vectorJ = VectorUtils.from(this.points[0], this.points[2]);
        final Vector vectorK = VectorUtils.from(this.points[0], this.points[4]);
        final Vector vectorV = VectorUtils.from(this.points[0], location);
        return (0D < vectorV.dot(vectorI) && vectorV.dot(vectorI) < vectorI.dot(vectorI))
                && (0D < vectorV.dot(vectorJ) && vectorV.dot(vectorJ) < vectorJ.dot(vectorJ))
                && (0D < vectorV.dot(vectorK) && vectorV.dot(vectorK) < vectorK.dot(vectorK));
    }

    public @NotNull String getId() {
        return id;
    }

    @NotNull
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
        this.points = this.getHitBoxPoints();
    }

    public @NotNull Location getTeleport() {
        return teleport;
    }

    public void setTeleport(@NotNull Location teleport) {
        this.teleport = teleport;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
        this.points = this.getHitBoxPoints();
    }

    public @NotNull Color getColor() {
        return color;
    }

    public void setColor(@NotNull Color color) {
        this.color = color;
    }

    public double getDepth() {
        return depth;
    }

    public void setDepth(double depth) {
        this.depth = depth;
        this.points = this.getHitBoxPoints();
    }

    public boolean isShowingHitBox() {
        return showHitBox;
    }

    public void setShowHitBox(boolean showHitBox) {
        this.showHitBox = showHitBox;
    }

}