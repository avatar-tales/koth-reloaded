package fr.avatarreturns.domination.game;

public enum GameStatus {

    INVALID(false),
    WAITING(true),
    GAME(true);

    private final boolean allowPlayer;

    GameStatus(final boolean allowPlayer) {
        this.allowPlayer = allowPlayer;
    }

    public boolean get() {
        return this.allowPlayer;
    }
}
