package fr.avatarreturns.domination.game;

import org.bukkit.Location;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FallBackPlayer {

    private final transient static List<FallBackPlayer> fallBackPlayers = new ArrayList<>();
    @NotNull
    private final UUID uuid;
    @NotNull
    private final Location location;
    @NotNull
    private final String teamName;

    public FallBackPlayer(final @NotNull UUID uuid, final @NotNull Location location) {
        this.uuid = uuid;
        this.location = location;
        this.teamName = "";
        fallBackPlayers.add(this);
    }


    public FallBackPlayer(final @NotNull UUID uuid, final @NotNull Location location, final @NotNull String teamName) {
        this.uuid = uuid;
        this.location = location;
        this.teamName = teamName;
        fallBackPlayers.add(this);
    }

    public static List<FallBackPlayer> getFallBackPlayers() {
        return fallBackPlayers;
    }

    public @NotNull UUID getUniqueId() {
        return this.uuid;
    }

    public @NotNull Location getLocation() {
        return this.location;
    }

    public @NotNull String getTeamName() {
        return this.teamName;
    }

}
