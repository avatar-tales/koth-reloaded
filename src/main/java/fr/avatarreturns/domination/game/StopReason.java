package fr.avatarreturns.domination.game;

public enum StopReason {

    COMMAND,
    TIMEOUT

}
