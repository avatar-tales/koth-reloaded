package fr.avatarreturns.domination.game;

import net.minecraft.world.entity.decoration.EntityArmorStand;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuppressWarnings({"unused"})
public class PlayerGame {

    private final UUID                   uuid;
    private final List<EntityArmorStand> allies;
    private int                          points;
    private int kills;
    private int deaths;
    private boolean beFallBackPlayer;
    private int timePlayed;
    private PlayerType status;

    public PlayerGame(final UUID uuid) {
        this.uuid = uuid;
        this.points = 0;
        this.kills = 0;
        this.deaths = 0;
        this.beFallBackPlayer = false;
        this.allies = new ArrayList<>();
    }

    public boolean isOnline() {
        return Bukkit.getOfflinePlayer(this.uuid).isOnline();
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(this.uuid);
    }

    public OfflinePlayer getOfflinePlayer() {
        return Bukkit.getOfflinePlayer(this.uuid);
    }

    public String getName() {
        return this.getOfflinePlayer().getName();
    }

    public void setBeFallBackPlayer(final boolean canBeFallBackPlayer) {
        this.beFallBackPlayer = canBeFallBackPlayer;
    }

    public boolean canBeFallBackPlayer() {
        return beFallBackPlayer;
    }

    public void sendMessage(final String message) {
        if (this.getOfflinePlayer().isOnline())
            this.getPlayer().sendMessage(message);
    }

    public UUID getUniqueId() {
        return uuid;
    }

    public int getPoints() {
        return points;
    }

    public void addPoints(final int points) {
        this.points += points;
    }

    public int getKills() {
        return kills;
    }

    public void addKills(final int kills) {
        this.kills += kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public void addDeaths(final int deaths) {
        this.deaths += deaths;
    }

    public int getTimePlayed() {
        return timePlayed;
    }

    public void addTime() {
        this.timePlayed++;
    }

    public PlayerType getStatus() {
        return status;
    }

    public void setStatus(PlayerType status) {
        this.status = status;
    }

    public List<EntityArmorStand> getAllies() {
        return this.allies;
    }
}
