package fr.avatarreturns.domination.game;

public enum PlayerType {

    WAITING(),
    PLAYING(),
    SPECTATE()

}
