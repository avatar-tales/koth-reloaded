package fr.avatarreturns.domination.game;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.*;

@SuppressWarnings({"unused"})
public class Squad {

    private final String name;
    private final Color color;
    private final String absoluteName;

    private final List<PlayerGame> players;
    private final List<PlayerGame> removedPlayers;
    private final Map<Plate, BossBar> bossBars;

    private int points;

    /**
     * @param domination The {@link Domination} of the squad
     * @param name       The name of the squad
     */
    public Squad(final Domination domination, final String name, final Color color) {
        this.name = ChatColor.translateAlternateColorCodes('&', "&" + new Random().nextInt(10) + name);
        this.color = color;
        this.absoluteName = name;
        this.players = new ArrayList<>();
        this.removedPlayers = new ArrayList<>();
        this.bossBars = new HashMap<>();
        for (final Plate plate : domination.getPlates()) {
            final BossBar bossBar = Bukkit.createBossBar("Plaque " + plate.getName(), BarColor.WHITE, BarStyle.SOLID);
            bossBars.put(plate, bossBar);
        }
        this.points = 0;
    }

    /**
     * @param player The {@link PlayerGame}
     */
    public void addPlayer(final PlayerGame player) {
        /*for (final PlayerGame playerGame : this.players) {
            final EntityArmorStand joined = this.getAllied(player);
            playerGame.getAllies().add(joined);
            PacketPlayOutSpawnEntity packet = new PacketPlayOutSpawnEntity(joined);
            ((CraftPlayer) playerGame.getPlayer()).getHandle().playerConnection.sendPacket(packet);
            ((CraftPlayer) playerGame.getPlayer()).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(joined.getId(), joined.getDataWatcher(), true));

            final EntityArmorStand joiner = this.getAllied(playerGame);
            player.getAllies().add(joiner);
            packet = new PacketPlayOutSpawnEntity(joiner);
            ((CraftPlayer) player.getPlayer()).getHandle().playerConnection.sendPacket(packet);
            ((CraftPlayer) player.getPlayer()).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(joiner.getId(), joiner.getDataWatcher(), true));
        }*/
        this.players.add(player);
        if (player.isOnline())
            player.getPlayer().getInventory().setHelmet(this.helmet(this.color));
        for (final BossBar bossBar : this.bossBars.values()) {
            bossBar.addPlayer(player.getPlayer());
        }
    }

    /**
     * @param player The {@link PlayerGame}
     */
    public void removePlayer(final PlayerGame player) {
        if (player.isOnline())
            player.getPlayer().getInventory().setHelmet(new ItemStack(Material.AIR));
        this.players.remove(player);
        for (final BossBar bossBar : this.bossBars.values()) {
            bossBar.removePlayer(player.getPlayer());
        }
        /*for (final EntityArmorStand entityArmorStand : player.getAllies()) {
            if (player.isOnline()) {
                PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(entityArmorStand.getId());
                ((CraftPlayer) player.getPlayer()).getHandle().playerConnection.sendPacket(packet);
            }
            if (entityArmorStand.isAlive())
                entityArmorStand.killEntity();
        }*/
        player.getAllies().clear();
    }

    /**
     * Add a point to the squad
     */
    public void addPoint() {
        this.points++;
    }

    /**
     * @return the points of the squad plus the points of all players in the squad
     */
    public int getPoints() {
        int points = 0;
        for (final PlayerGame player : this.players) {
            points += player.getPoints();
        }
        return points + this.points;
    }

    /**
     * @return the points of the squad
     */
    public int getAbsolutePoints() {
        return this.points;
    }

    /**
     * @return the numbers of players who are playing in this {@link Squad}
     */
    public int getSize() {
        return this.players.size();
    }

    /*public static EntityArmorStand generateArmorStand(final Location location) {
        try {
            final WorldServer s = ((CraftWorld) location.getWorld()).getHandle();
            final double x = location.clone().getX();
            final double y = location.clone().getY() + 2.75;
            final double z = location.clone().getZ();
            EntityArmorStand stand = new EntityArmorStand(EntityTypes.ARMOR_STAND, s.getMinecraftWorld());

            stand.setLocation(x, y, z, 0f, 0f);
            stand.setCustomName(IChatBaseComponent.ChatSerializer.a("{\"text\":\" " + ChatColor.translateAlternateColorCodes('&', "&2&l« &a&lAllié &2&l»") + "\"}"));
            stand.setCustomNameVisible(true);
            stand.setNoGravity(true);
            stand.setBasePlate(false);
            stand.setSilent(true);
            stand.setSmall(true);
            stand.setInvulnerable(true);
            stand.setInvisible(true);

            return stand;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public EntityArmorStand getAllied(final PlayerGame player) {
        try {
            return generateArmorStand(player.getPlayer().getLocation());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }*/

    private ItemStack helmet(final Color color) {
        final ItemStack item = new ItemStack(Material.LEATHER_HELMET, 1);
        final ItemMeta meta = item.getItemMeta();
        meta.setUnbreakable(true);
        meta.setDisplayName("§6Casque VR");
        final LeatherArmorMeta lmeta = (LeatherArmorMeta)meta;
        ((LeatherArmorMeta)meta).setColor(color);
        item.setItemMeta(lmeta);
        return item;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

    public String getAbsoluteName() {
        return absoluteName;
    }

    public List<PlayerGame> getPlayers() {
        return players;
    }

    public List<PlayerGame> getRemovedPlayers() {
        return removedPlayers;
    }

    public Map<Plate, BossBar> getBossBars() {
        return bossBars;
    }
}
