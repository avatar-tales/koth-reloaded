package fr.avatarreturns.domination.game;

import fr.avatarreturns.domination.manager.config.DefaultConfig;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.Main;
import fr.avatarreturns.domination.utils.PluginMessage;
import fr.avatarreturns.domination.utils.Utils;
import net.minecraft.network.protocol.game.PacketPlayOutEntityDestroy;
import net.minecraft.network.protocol.game.PacketPlayOutNamedSoundEffect;
import net.minecraft.sounds.SoundCategory;
import net.minecraft.sounds.SoundEffects;
import net.minecraft.world.entity.decoration.EntityArmorStand;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BossBar;
import org.bukkit.craftbukkit.v1_18_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@SuppressWarnings({"unused"})
public class Domination {


    private final static List<Domination> dominations = new ArrayList<>();

    private final UUID uuid;
    private final List<Circle> circles;
    private final List<Plate> plates;
    private String name;
    private int timer;
    private int teamNumber;
    private int pointPerKill;
    private int pointPerDeath;
    private int secondsToCapture;
    private double yenPerPoints;
    private double reward;
    private double minPointsReward;
    private double minParticipationReward;
    private int      spectateDuration;
    private Location spawn;
    private Location returnLocation;
    private String channelId;

    private transient GameStatus status;
    private transient List<SquadName> teamsNameAvailable;
    private transient List<SquadColor> teamsColorAvailable;
    private transient List<Squad> teams;
    private transient List<PlayerGame> players;
    private transient int discount;
    private transient int step;

    public Domination(final String name) {
        this.uuid = UUID.randomUUID();
        this.name = name;
        this.timer = DefaultConfig.getTimer();
        this.teamNumber = DefaultConfig.getTeamNumber();
        this.yenPerPoints = DefaultConfig.getYenPerPoints();
        this.reward = DefaultConfig.getReward();
        this.minPointsReward = DefaultConfig.getMinPointsReward();
        this.minParticipationReward = DefaultConfig.getMinParticipationReward();
        this.pointPerKill = DefaultConfig.getPointPerKill();
        this.pointPerDeath = DefaultConfig.getPointPerDeath();
        this.secondsToCapture = DefaultConfig.getSecondsToCapture();
        this.spectateDuration = DefaultConfig.getSpectateDuration();
        this.channelId = DefaultConfig.getChannelId();
        this.circles = new ArrayList<>();
        this.plates = new ArrayList<>();
        this.init();
    }

    /**
     * Get all instances of Domination
     *
     * @return List of all instances of Domination
     */
    public static List<Domination> getDominations() {
        return dominations;
    }

    public static Domination getDomination(final Player player) {
        for (final Domination domination : getDominations()) {
            for (final PlayerGame playerGame : domination.players) {
                if (playerGame.getUniqueId().equals(player.getUniqueId()))
                    return domination;
            }
        }
        return null;
    }

    public static Domination getDomination(final PlayerGame player) {
        for (final Domination domination : getDominations()) {
            if (domination.players.contains(player))
                return domination;
        }
        return null;
    }

    public static Domination getDomination(final String name) {
        for (final Domination domination : getDominations()) {
            if (domination.name.equalsIgnoreCase(name))
                return domination;
        }
        return null;
    }

    public static Domination getDomination(final UUID uuid) {
        for (final Domination domination : getDominations()) {
            if (domination.getUniqueId().equals(uuid))
                return domination;
        }
        return null;
    }

    public void init() {
        this.status = GameStatus.WAITING;
        if (this.isInvalid())
            this.status = GameStatus.INVALID;
        this.teams = new ArrayList<>();
        this.players = new ArrayList<>();
        this.teamsNameAvailable = new ArrayList<>();
        for (final SquadName squadName : SquadName.values()) {
            final boolean add = this.teamsNameAvailable.add(squadName);
        }
        this.teamsColorAvailable = new ArrayList<>();
        for (final SquadColor squadColor : SquadColor.values()) {
            final boolean add = this.teamsColorAvailable.add(squadColor);
        }
        this.discount = this.timer;
        this.step = 0;
        for (final Plate plate : this.plates) {
            plate.setValue(0);
            plate.setCapturing(null);
            plate.setCaptured(null);
        }
    }

    public boolean addPlayer(final Player player, final String squadName) {
        if ((!this.status.get() || isInvalid()) && isInvalid()) {
            player.sendMessage(Lang.ERROR_JOIN_CANT.get());
            return false;
        }
        Squad squad = null;
        for (final Squad squads : this.teams) {
            if (squads.getAbsoluteName().equalsIgnoreCase(squadName)) {
                squad = squads;
                break;
            }
        }
        if (squad == null) {
            return this.addPlayer(player);
        }
        player.teleport(this.spawn);
        if (this.status.equals(GameStatus.GAME)) {
            PlayerGame playerGame = null;
            for (final Squad squads : this.teams) {
                for (final PlayerGame playersGame : squad.getRemovedPlayers()) {
                    if (playersGame.getUniqueId().equals(player.getUniqueId()))
                        playerGame = playersGame;
                }
            }
            if (playerGame != null) {
                for (final Squad squads : this.teams) {
                    if (squads.getRemovedPlayers().contains(playerGame)) {
                        squads.getRemovedPlayers().remove(playerGame);
                        squad.addPlayer(playerGame);
                        break;
                    }
                }
                return joiningGame(player, playerGame);
            }
            playerGame = new PlayerGame(player.getUniqueId());
            this.players.add(playerGame);
            squad.addPlayer(playerGame);
            for (final PlayerGame playerGame1 : this.players) {
                playerGame1.sendMessage(Lang.INFO_JOIN_IN_GAME.get(player.getName(), squad.getName()));
            }
            return true;
        }
        final PlayerGame playerGame = new PlayerGame(player.getUniqueId());
        this.players.add(playerGame);
        squad.addPlayer(playerGame);
        playerGame.setStatus(PlayerType.WAITING);
        for (final PlayerGame playerGame1 : this.players) {
            playerGame1.sendMessage(Lang.INFO_JOIN.get(player.getName()));
        }
        return true;
    }

    private boolean joiningGame(Player player, PlayerGame playerGame) {
        this.players.add(playerGame);
        playerGame.setBeFallBackPlayer(false);
        playerGame.setStatus(PlayerType.PLAYING);
        for (final PlayerGame playerGame1 : this.players) {
            playerGame1.sendMessage(Lang.INFO_JOIN_RETRIEVE.get(player.getName()));
        }
        return true;
    }

    public boolean addPlayer(final Player player) {
        if ((!this.status.get() || isInvalid()) && isInvalid()) {
            player.sendMessage(Lang.ERROR_JOIN_CANT.get());
            return false;
        }
        player.teleport(this.spawn);
        if (this.status.equals(GameStatus.GAME)) {
            PlayerGame playerGame = null;
            for (final Squad squad : this.teams) {
                for (final PlayerGame playersGame : squad.getRemovedPlayers()) {
                    if (playersGame.getUniqueId().equals(player.getUniqueId()))
                        playerGame = playersGame;
                }
            }
            if (playerGame != null) {
                for (final Squad squad : this.teams) {
                    if (squad.getRemovedPlayers().contains(playerGame)) {
                        squad.getRemovedPlayers().remove(playerGame);
                        squad.addPlayer(playerGame);
                    }
                }
                return joiningGame(player, playerGame);
            }
            playerGame = new PlayerGame(player.getUniqueId());
            this.players.add(playerGame);
            final Squad squad = this.getMinSquad();
            squad.addPlayer(playerGame);
            for (final PlayerGame playerGame1 : this.players) {
                playerGame1.sendMessage(Lang.INFO_JOIN_IN_GAME.get(player.getName(), squad.getName()));
            }
            return true;
        }
        final PlayerGame playerGame = new PlayerGame(player.getUniqueId());
        this.players.add(playerGame);
        playerGame.setStatus(PlayerType.WAITING);
        for (final PlayerGame playerGame1 : this.players) {
            playerGame1.sendMessage(Lang.INFO_JOIN.get(player.getName()));
        }
        return true;
    }

    public boolean removePlayer(final Player player) {
        final PlayerGame playerGame = this.getPlayerGame(player);
        if (playerGame != null) {
            for (final PlayerGame playerGames : this.players) {
                playerGames.sendMessage(Lang.INFO_LEAVE.get(player.getName()));
            }
            this.players.remove(playerGame);
            for (final Squad squad : this.teams) {
                if (squad.getPlayers().contains(playerGame)) {
                    squad.removePlayer(playerGame);
                    squad.getRemovedPlayers().add(playerGame);
                }
            }
        }
        return true;
    }

    public void start() {
        if (this.players.size() < 2) {
            this.players.parallelStream().forEach(player -> player.sendMessage(Lang.ERROR_NOT_ENOUGH.get(this.name)));
            Main.getInstance().getServer().getConsoleSender().sendMessage(Lang.ERROR_NOT_ENOUGH.get(this.name));
            return;
        }
        this.plates.parallelStream().forEach(plate -> plate.setValue(0D));
        final int teamNumber = Math.min(this.players.size(), this.teamNumber);
        final int maxPlayers = (int) Math.ceil((float) this.players.size() / (float) teamNumber);
        final List<PlayerGame> players = this.shuffle(this.players);
        for (final Squad squad : teams) {
            for (final PlayerGame playerGame : squad.getPlayers()) {
                players.remove(playerGame);
            }
        }
        for (int i = 0; i < teamNumber; i++) {
            final Squad squad = new Squad(this, this.teamsNameAvailable.remove(new Random().nextInt(this.teamsNameAvailable.size())).name(), this.teamsColorAvailable.remove(new Random().nextInt(this.teamsColorAvailable.size())).getColor());
            for (int n = 0; n < maxPlayers; n++) {
                if (players.size() <= 0)
                    break;
                squad.addPlayer(players.remove(new Random().nextInt(Math.abs(players.size()))));

            }
            this.teams.add(squad);
        }
        for (final Squad squad : this.teams) {
            for (final PlayerGame playerGame : squad.getPlayers()) {
                final StringBuilder stringBuilder = new StringBuilder();
                final List<PlayerGame> others = Utils.copy(squad.getPlayers());
                others.remove(playerGame);
                for (int j = 0; j < others.size(); j++) {
                    stringBuilder.append("&6").append(others.get(j).getName());
                    if (j < others.size() - 1) {
                        stringBuilder.append("&7, ");
                    }
                }
                playerGame.sendMessage(Lang.INFO_ENTER_TEAM.get(squad.getName(), stringBuilder.toString()));
            }
        }
        for (final PlayerGame playerGame : this.players) {
            playerGame.setStatus(PlayerType.PLAYING);
            playerGame.sendMessage(Lang.INFO_START.get());
            final double x = playerGame.getPlayer().getLocation().getX();
            final double y = playerGame.getPlayer().getLocation().getY();
            final double z = playerGame.getPlayer().getLocation().getZ();
            final PacketPlayOutNamedSoundEffect packet = new PacketPlayOutNamedSoundEffect(SoundEffects.pw, SoundCategory.j, x, y, z, 100, 1);
            ((CraftPlayer) playerGame.getPlayer()).getHandle().b.a(packet);
        }
        this.status = GameStatus.GAME;
    }

    private void stop() {
        this.stop(StopReason.TIMEOUT);
    }

    public void stop(final StopReason stopReason) {
        try {
            final List<Squad> winners = new ArrayList<>();
            winners.add(this.teams.get(0));
            for (int i = 1; i < this.teams.size(); i++) {
                if (winners.get(0).getPoints() == this.teams.get(i).getPoints()) {
                    winners.add(this.teams.get(i));
                    continue;
                }
                if (winners.get(0).getPoints() < this.teams.get(i).getPoints()) {
                    winners.clear();
                    winners.add(this.teams.get(i));
                }
            }
            final StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < winners.size(); i++) {
                stringBuilder.append("&6").append(winners.get(i).getName()).append(" (").append(winners.get(i).getPoints()).append(")");
                if (i < winners.size() - 1)
                    stringBuilder.append("&7, ");
            }
            if (stopReason.equals(StopReason.TIMEOUT)) {
                final Map<Squad, Integer> squadShort = shortSquads(Utils.copy(this.teams));
                for (final Squad squad : this.teams) {
                    double moneyPerPlayers = (this.yenPerPoints * (float) squad.getAbsolutePoints()) / winners.size();
                    for (final PlayerGame playerGame : squad.getPlayers()) {
                        double money = format(Math.min(((float) playerGame.getTimePlayed() / (float) this.timer) * moneyPerPlayers, 7000));
                        Main.getEcon().depositPlayer(playerGame.getPlayer(), (money < 0 ? 0 : money));
                        playerGame.sendMessage(Lang.INFO_GET_MONEY.get((money < 0 ? "0" : (stringFormat(money) + (money >= 7000 ? "MAX" : "")))));
                        money = format(Math.min(((float) playerGame.getTimePlayed() / (float) this.timer) * ((1d/(squadShort.get(squad)) * 500) + Math.max(this.pointPerKill * playerGame.getKills() - this.pointPerDeath * playerGame.getDeaths(), 0) * yenPerPoints), 5000));
                        Main.getEcon().depositPlayer(playerGame.getPlayer(), (money < 0 ? 0 : money));
                        playerGame.sendMessage(Lang.INFO_GET_PARTMONEY.get((money < 0 ? "0" : (stringFormat(money) + (money >= 5000 ? "MAX" : "")))));
                    }
                    for (final PlayerGame playerGame : squad.getRemovedPlayers()) {
                        double money = format(Math.min(((float) playerGame.getTimePlayed() / (float) this.timer) * moneyPerPlayers, 7000));
                        Main.getEcon().depositPlayer(playerGame.getOfflinePlayer(), (money < 0 ? 0 : money));
                        money = format(Math.min(((float) playerGame.getTimePlayed() / (float) this.timer) * (1d/squadShort.get(squad) * 500 + Math.max(this.pointPerKill * playerGame.getKills() - this.pointPerDeath * playerGame.getDeaths(), 0)), 5000));
                        Main.getEcon().depositPlayer(playerGame.getOfflinePlayer(), (money < 0 ? 0 : money));
                    }
                }
            }
            for (final PlayerGame playerGame : this.players) {
                playerGame.sendMessage(Lang.INFO_WINNER.get(stringBuilder.toString()));
            }
            for (final Squad squad : this.teams) {
                for (final PlayerGame playerGame : squad.getRemovedPlayers()) {
                    if (playerGame.canBeFallBackPlayer())
                        new FallBackPlayer(playerGame.getUniqueId(), this.returnLocation, stringBuilder.toString());
                }
                for (final PlayerGame playerGame : Utils.copy(squad.getPlayers())) {
                    playerGame.getPlayer().setGameMode(GameMode.SURVIVAL);
                    playerGame.getPlayer().setHealth(20f);
                    playerGame.getPlayer().setFoodLevel(20);
                    playerGame.getPlayer().teleport(this.returnLocation, PlayerTeleportEvent.TeleportCause.PLUGIN);
                    for (BossBar bossBar : squad.getBossBars().values()) {
                        bossBar.removePlayer(playerGame.getPlayer());
                    }
                    final double x = playerGame.getPlayer().getLocation().getX();
                    final double y = playerGame.getPlayer().getLocation().getY();
                    final double z = playerGame.getPlayer().getLocation().getZ();
                    ((CraftPlayer) playerGame.getPlayer()).getHandle().b.a(new PacketPlayOutNamedSoundEffect(SoundEffects.fY, SoundCategory.j, x, y, z, 1000, 0.4f));
                    ((CraftPlayer) playerGame.getPlayer()).getHandle().b.a(new PacketPlayOutNamedSoundEffect(SoundEffects.gb, SoundCategory.j, x, y, z, 1000, 1.5f));
                    ((CraftPlayer) playerGame.getPlayer()).getHandle().b.a(new PacketPlayOutNamedSoundEffect(SoundEffects.fS, SoundCategory.j, x, y, z, 1000, 1));
                    for (final EntityArmorStand entityArmorStand : playerGame.getAllies()) {
                        // Method ae is the new get id method (why ? I don't know)
                        PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(entityArmorStand.ae());
                        ((CraftPlayer) playerGame.getPlayer()).getHandle().b.a(packet);
                        // bl() <=> isAlive()
                        if (entityArmorStand.bl())
                            // ag() <=> kill()
                            entityArmorStand.ag();
                    }
                    this.players.remove(playerGame);
                }
            }
            new Thread(() -> {
                if (!this.channelId.equalsIgnoreCase("") && !this.channelId.equalsIgnoreCase("none"))
                    PluginMessage.sendResult(this.channelId, this.name, Utils.copy(this.teams));
                this.init();
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void progressCircle() {
        for (final Circle circle : this.circles) {
            circle.render();
            if (this.step == 0) {
                Objects.requireNonNull(circle.getLocation().getWorld()).spawnParticle(Particle.CLOUD, circle.getLocation(), 5);
            }
            for (final Entity entity : Objects.requireNonNull(circle.getLocation().getWorld()).getNearbyEntities(circle.getLocation(), circle.getRadius(), circle.getRadius(), circle.getRadius())) {
                if (circle.isLocationInCircle(entity.getLocation())) {
                    entity.teleport(circle.getTeleport());
                    if (entity instanceof Player) {
                        final double x = entity.getLocation().getX();
                        final double y = entity.getLocation().getY();
                        final double z = entity.getLocation().getZ();
                        final PacketPlayOutNamedSoundEffect packet = new PacketPlayOutNamedSoundEffect(SoundEffects.cY, SoundCategory.j, x, y, z, 100, 1);
                        ((CraftPlayer) entity).getHandle().b.a(packet);
                    }
                }
            }
        }
    }

    public void progressPlate() {
        plates:
        for (final Plate plate : this.plates) {
            if (plate.getCaptured() != null)
                plate.getCaptured().addPoint();
            final List<Squad> squads = plate.getSquadOn(this.teams);
            if (squads.size() == 0) {
                if (plate.getCaptured() == null && plate.getCapturing() == null) {
                    for (final Squad squad : this.teams) {
                        squad.getBossBars().get(plate).setProgress(0);
                        squad.getBossBars().get(plate).setColor(BarColor.WHITE);
                    }
                } else {
                    if (plate.getCaptured() != null) {
                        plate.getCaptured().getBossBars().get(plate).setColor(BarColor.PINK);
                        plate.getCaptured().getBossBars().get(plate).setProgress(1);
                    }
                    if (plate.getCapturing() != null) {
                        if (plate.getValue() > 0) {
                            plate.addValue(-1f / this.secondsToCapture);
                            plate.getCapturing().getBossBars().get(plate).setProgress(plate.getValue());
                            for (final Squad squad : this.teams) {
                                if (squad.equals(plate.getCapturing())) {
                                    squad.getBossBars().get(plate).setColor(BarColor.YELLOW);
                                    continue;
                                } else if (squad == plate.getCaptured()) {
                                    squad.getBossBars().get(plate).setProgress(1 - plate.getValue());
                                    squad.getBossBars().get(plate).setColor(BarColor.RED);
                                    continue;
                                }
                                squad.getBossBars().get(plate).setProgress(plate.getValue());
                                squad.getBossBars().get(plate).setColor(BarColor.PURPLE);
                            }
                        }
                        if (plate.getValue() <= 0) {
                            if (plate.getCaptured() != null) {
                                plate.getCaptured().getBossBars().get(plate).setColor(BarColor.PINK);
                                plate.getCaptured().getBossBars().get(plate).setProgress(1);
                                for (final Squad other : this.teams) {
                                    if (other.equals(plate.getCaptured())) continue;
                                    other.getBossBars().get(plate).setProgress(plate.getValue());
                                    other.getBossBars().get(plate).setColor(BarColor.PURPLE);
                                }
                            } else {
                                for (final Squad squad1 : this.teams) {
                                    squad1.getBossBars().get(plate).setProgress(0);
                                    squad1.getBossBars().get(plate).setColor(BarColor.WHITE);
                                }
                            }
                            plate.setCapturing(null);
                        }
                    }
                }
            } else if (squads.size() == 1) {
                if (plate.getCaptured() == null && plate.getCapturing() == null) {
                    for (final Squad squad : this.teams) {
                        if (squad.equals(squads.get(0))) {
                            plate.setValue(0);
                            plate.setCapturing(squad);
                            squad.getBossBars().get(plate).setProgress(0);
                            squad.getBossBars().get(plate).setColor(BarColor.YELLOW);
                            for (final Squad others : this.teams) {
                                if (others.equals(squad)) continue;
                                others.getBossBars().get(plate).setProgress(0);
                                others.getBossBars().get(plate).setColor(BarColor.PURPLE);
                            }
                            continue plates;
                        }
                    }
                } else if (plate.getCaptured() == null && plate.getCapturing() != null) {
                    for (final Squad squad : this.teams) {
                        if (plate.getCapturing().equals(squad) && squad.equals(squads.get(0)) && plate.getValue() < 1) {
                            plate.addValue(1f / this.secondsToCapture);
                            squad.getBossBars().get(plate).setProgress(plate.getValue());
                            squad.getBossBars().get(plate).setColor(BarColor.YELLOW);
                            for (final Squad other : this.teams) {
                                if (other.equals(squad)) continue;
                                other.getBossBars().get(plate).setColor(BarColor.PURPLE);
                                other.getBossBars().get(plate).setProgress(plate.getValue());
                            }
                            continue plates;
                        } else if (plate.getCapturing().equals(squad) && squad.equals(squads.get(0)) && plate.getValue() >= 1) {
                            plate.setValue(0);
                            plate.setCaptured(squad);
                            plate.setCapturing(null);
                            capAllPlates();
                            for (final Squad other : this.teams) {
                                for (final PlayerGame playerGame : other.getPlayers()) {
                                    playerGame.sendMessage(Lang.INFO_TAKE_PLATE.get(squad.getName(), plate.getName()));
                                }
                                if (other.equals(squad)) {
                                    other.getBossBars().get(plate).setProgress(1);
                                    other.getBossBars().get(plate).setColor(BarColor.GREEN);
                                    continue;
                                }
                                other.getBossBars().get(plate).setProgress(plate.getValue());
                                other.getBossBars().get(plate).setColor(BarColor.PURPLE);
                            }
                            continue plates;
                        } else if (squad.equals(squads.get(0))) {
                            plate.setValue(0);
                            plate.setCapturing(squad);
                            for (final Squad other : this.teams) {
                                if (other.equals(squad)) {
                                    other.getBossBars().get(plate).setProgress(0);
                                    other.getBossBars().get(plate).setColor(BarColor.YELLOW);
                                } else {
                                    squad.getBossBars().get(plate).setProgress(plate.getValue());
                                    other.getBossBars().get(plate).setColor(BarColor.PURPLE);
                                }
                            }
                            continue plates;
                        }
                    }
                } else if (plate.getCaptured() != null && plate.getCapturing() == null) {
                    for (final Squad squad : this.teams) {
                        if (squad.equals(squads.get(0)) && squad.equals(plate.getCaptured())) {
                            squad.getBossBars().get(plate).setProgress(1);
                            squad.getBossBars().get(plate).setColor(BarColor.GREEN);
                            continue plates;
                        }
                        if (squad.equals(squads.get(0)) && !squad.equals(plate.getCaptured())) {
                            plate.setCapturing(squad);
                            squad.getBossBars().get(plate).setColor(BarColor.YELLOW);
                            for (final Squad others : this.teams) {
                                if (others.equals(squad))
                                    continue;
                                if (others.equals(plate.getCaptured()))
                                    others.getBossBars().get(plate).setColor(BarColor.RED);
                                else {
                                    others.getBossBars().get(plate).setProgress(plate.getValue());
                                    others.getBossBars().get(plate).setColor(BarColor.PURPLE);
                                }
                            }
                            continue plates;
                        }
                    }
                } else if (plate.getCaptured() != null && plate.getCapturing() != null) {
                    for (final Squad squad : this.teams) {
                        if (squad.equals(plate.getCaptured()) && squads.get(0).equals(squad)) {
                            if (plate.getValue() <= 0) {
                                squad.getBossBars().get(plate).setColor(BarColor.GREEN);
                                plate.setValue(0);
                                plate.setCapturing(null);
                                for (final Squad others : this.teams) {
                                    if (others.equals(plate.getCaptured())) continue;
                                    others.getBossBars().get(plate).setColor(BarColor.PURPLE);
                                    others.getBossBars().get(plate).setProgress(plate.getValue());
                                }
                            } else {
                                squad.getBossBars().get(plate).setColor(BarColor.RED);
                                plate.addValue(-1f / this.secondsToCapture);
                                squad.getBossBars().get(plate).setProgress(1 - plate.getValue());
                                for (final Squad others : this.teams) {
                                    if (others.equals(plate.getCaptured())) continue;
                                    if (others.equals(plate.getCapturing())) {
                                        others.getBossBars().get(plate).setColor(BarColor.BLUE);
                                        others.getBossBars().get(plate).setProgress(plate.getValue());
                                        continue;
                                    }
                                    others.getBossBars().get(plate).setColor(BarColor.PURPLE);
                                    others.getBossBars().get(plate).setProgress(plate.getValue());
                                }
                            }
                            continue plates;
                        }
                        if (squad.equals(plate.getCapturing()) && squads.get(0).equals(squad)) {
                            if (plate.getValue() >= 1) {
                                squad.getBossBars().get(plate).setColor(BarColor.GREEN);
                                squad.getBossBars().get(plate).setProgress(1);
                                plate.setValue(0);
                                plate.setCaptured(squad);
                                plate.setCapturing(null);
                                capAllPlates();
                                for (final Squad others : this.teams) {
                                    for (final PlayerGame playerGame : others.getPlayers()) {
                                        playerGame.sendMessage(Lang.INFO_TAKE_PLATE.get(squad.getName(), plate.getName()));
                                    }
                                    if (others.equals(plate.getCaptured())) continue;
                                    others.getBossBars().get(plate).setColor(BarColor.PURPLE);
                                    others.getBossBars().get(plate).setProgress(plate.getValue());
                                }
                            } else {
                                plate.addValue(1f / this.secondsToCapture);
                                squad.getBossBars().get(plate).setProgress(plate.getValue());
                                squad.getBossBars().get(plate).setColor(BarColor.YELLOW);
                                for (final Squad others : this.teams) {
                                    if (others.equals(plate.getCaptured())) {
                                        others.getBossBars().get(plate).setColor(BarColor.RED);
                                        others.getBossBars().get(plate).setProgress(1 - plate.getValue());
                                        continue;
                                    }
                                    if (others.equals(squad)) continue;
                                    others.getBossBars().get(plate).setColor(BarColor.PURPLE);
                                    others.getBossBars().get(plate).setProgress(plate.getValue());
                                }
                            }
                            continue plates;
                        }
                        if (!squad.equals(plate.getCaptured()) && !squad.equals(plate.getCapturing()) && squad.equals(squads.get(0))) {
                            plate.setCapturing(squad);
                            plate.setValue(0);
                            squad.getBossBars().get(plate).setProgress(plate.getValue());
                            squad.getBossBars().get(plate).setColor(BarColor.YELLOW);
                            for (final Squad others : this.teams) {
                                if (others.equals(plate.getCaptured())) {
                                    squad.getBossBars().get(plate).setColor(BarColor.RED);
                                    squad.getBossBars().get(plate).setProgress(1 - plate.getValue());
                                }
                                if (others.equals(squad)) continue;
                                others.getBossBars().get(plate).setColor(BarColor.PURPLE);
                                others.getBossBars().get(plate).setProgress(plate.getValue());
                            }
                        }
                    }
                }
            } else {
                if (plate.getCaptured() == null && plate.getCapturing() == null) {
                    for (final Squad squad : this.teams) {
                        squad.getBossBars().get(plate).setProgress(0);
                        squad.getBossBars().get(plate).setColor(BarColor.WHITE);
                    }
                } else if (plate.getCaptured() != null && plate.getCapturing() == null) {
                    for (final Squad squad : this.teams) {
                        if (squad.equals(plate.getCaptured())) {
                            squad.getBossBars().get(plate).setProgress(1 - plate.getValue());
                            squad.getBossBars().get(plate).setColor(BarColor.RED);
                            continue;
                        }
                        squad.getBossBars().get(plate).setColor(BarColor.PURPLE);
                        squad.getBossBars().get(plate).setProgress(plate.getValue());
                    }
                } else if (plate.getCaptured() == null && plate.getCapturing() != null) {
                    for (final Squad squad : this.teams) {
                        if (squad.equals(plate.getCapturing())) {
                            squad.getBossBars().get(plate).setProgress(plate.getValue());
                            squad.getBossBars().get(plate).setColor(BarColor.BLUE);
                            continue;
                        }
                        squad.getBossBars().get(plate).setColor(BarColor.PURPLE);
                        squad.getBossBars().get(plate).setProgress(plate.getValue());
                    }
                } else if (plate.getCaptured() != null && plate.getCapturing() != null) {
                    for (final Squad squad : this.teams) {
                        if (squad.equals(plate.getCaptured())) {
                            squad.getBossBars().get(plate).setProgress(1 - plate.getValue());
                            squad.getBossBars().get(plate).setColor(BarColor.RED);
                            continue;
                        }
                        if (squad.equals(plate.getCapturing())) {
                            squad.getBossBars().get(plate).setProgress(plate.getValue());
                            squad.getBossBars().get(plate).setColor(BarColor.BLUE);
                            continue;
                        }
                        squad.getBossBars().get(plate).setProgress(plate.getValue());
                        squad.getBossBars().get(plate).setColor(BarColor.PURPLE);
                    }
                }
            }
        }
    }

    private void capAllPlates() {
        if (this.plates.size() <= 1)
            return;
        Squad squad = null;
        int i = 0;
        for (final Plate plate : this.plates) {
            if (squad == null) {
                squad = plate.getCaptured();
                i++;
            } else {
                if (squad.equals(plate.getCaptured()))
                    i++;
                else
                    break;
            }
        }
        if (i == this.plates.size() && squad != null) {
            for (final PlayerGame playerGame : this.players) {
                playerGame.sendMessage(Lang.WARNING_SQUAD_DOMINATE.get(squad.getName(), String.valueOf(i)));
            }
        }
    }

    private void progressOther() {
        for (final PlayerGame playerGame : this.players) {
            if (playerGame.isOnline())
                playerGame.addTime();
        }
        final List<Integer> ints = Arrays.asList(1, 2, 3, 4, 5, 30, 60, 5 * 60, 10 * 60, 15 * 60, 30 * 60, 45 * 60, 60 * 60, 90 * 60, 2 * 60 * 60, this.timer);
        if (ints.contains(this.discount)) {
            for (final PlayerGame playerGame : this.players) {
                playerGame.sendMessage(Lang.INFO_TIMER.get(String.valueOf(this.discount)));
                final double x = playerGame.getPlayer().getLocation().getX();
                final double y = playerGame.getPlayer().getLocation().getY();
                final double z = playerGame.getPlayer().getLocation().getZ();
                final PacketPlayOutNamedSoundEffect packetSound = new PacketPlayOutNamedSoundEffect((this.discount > 1 ? SoundEffects.ov : SoundEffects.ou), SoundCategory.j, x, y, z, 300, 0.75f);
                ((CraftPlayer) playerGame.getPlayer()).getHandle().b.a(packetSound);
            }
        }
    }

    /*private void progressAllied() {
        for (final Squad squad : this.teams) {
            for (final PlayerGame playerGame : squad.getPlayers()) {
                final List<PlayerGame> allies = Utils.copy(squad.getPlayers());
                allies.remove(playerGame);
                final List<EntityArmorStand> toRemove = new ArrayList<>();
                final int max = playerGame.getAllies().size();
                for (int i = 0; i < max; i++) {
                    if (i >= allies.size()) {
                        toRemove.add(playerGame.getAllies().get(i));
                        continue;
                    }
                    final Location location = allies.get(i).getPlayer().getLocation();
                    double y = location.getY() + 2.75;
                    if (allies.get(i).getPlayer().isSneaking()) {
                        y = location.getY() + 2.5;
                    }
                    EntityArmorStand entityArmorStand = playerGame.getAllies().get(i);
                    if (entityArmorStand == null)
                        entityArmorStand = squad.getAllied(allies.get(i));
                    if (entityArmorStand.getBukkitEntity().getLocation().distance(playerGame.getPlayer().getLocation()) >= 96) {
                        ((CraftPlayer) playerGame.getPlayer()).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(entityArmorStand.getId()));
                        entityArmorStand.getBukkitEntity().remove();
                        entityArmorStand = squad.getAllied(allies.get(i));
                        ((CraftPlayer) playerGame.getPlayer()).getHandle().playerConnection.sendPacket(new PacketPlayOutSpawnEntityLiving(entityArmorStand));
                        ((CraftPlayer) playerGame.getPlayer()).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(entityArmorStand.getId(), entityArmorStand.getDataWatcher(), true));
                        playerGame.getAllies().set(i, entityArmorStand);
                    }
                    if (allies.get(i) == null) {
                        ((CraftPlayer) playerGame.getPlayer()).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(entityArmorStand.getId()));
                        entityArmorStand.getBukkitEntity().remove();
                        continue;
                    }
                    entityArmorStand.setCustomNameVisible(!allies.get(i).getStatus().equals(PlayerType.SPECTATE));
                    entityArmorStand.getBukkitEntity().teleport(location.clone().add(0, y - location.getY(), 0));
                    ((CraftPlayer) playerGame.getPlayer()).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityTeleport(entityArmorStand));
                    ((CraftPlayer) playerGame.getPlayer()).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(entityArmorStand.getId(), entityArmorStand.getDataWatcher(), true));
                }
                for (final EntityArmorStand entityArmorStand : toRemove) {
                    PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(entityArmorStand.getId());
                    ((CraftPlayer) playerGame.getPlayer()).getHandle().playerConnection.sendPacket(packet);
                    if (entityArmorStand.isAlive())
                        entityArmorStand.killEntity();
                    playerGame.getAllies().remove(entityArmorStand);
                }
            }
        }

    }*/

    public void progress() {
        if (this.status.equals(GameStatus.GAME)) {
            if (this.discount == 0) {
                Bukkit.getScheduler().runTask(Main.getInstance(), (@NotNull Runnable) this::stop);
                return;
            }
            Bukkit.getScheduler().runTask(Main.getInstance(), this::progressCircle);
            //Bukkit.getScheduler().runTask(Main.getInstance(), this::progressAllied);
            if (this.step % 20 == 0) {
                Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), this::progressPlate);
                Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), this::progressOther);
                this.discount--;
            }
            step++;
        }
    }

    public UUID getUniqueId() {
        return this.uuid;
    }

    @Nullable
    public Squad getPlayerSquad(final PlayerGame playerGame) {
        for (final Squad squad : this.teams) {
            if (squad.getPlayers().contains(playerGame))
                return squad;
        }
        return null;
    }

    public Squad getMinSquad() {
        Squad squad = this.teams.get(0);
        for (int i = 1; i < this.teams.size(); i++) {
            final Squad look = this.teams.get(i);
            if (squad.getSize() > look.getSize())
                squad = look;
        }
        return squad;
    }

    private List<PlayerGame> shuffle(final List<PlayerGame> data) {
        final List<PlayerGame> toShuffle = Utils.copy(data);
        final List<PlayerGame> shuffled = new ArrayList<>();
        while (!toShuffle.isEmpty()) {
            shuffled.add(toShuffle.remove(new Random().nextInt(toShuffle.size())));
        }
        return shuffled;
    }

    private double format(final double dOuble) {
        final int decimals = (int) Math.pow(10, 2);
        return (double) ((int) Math.round(dOuble * decimals)) / (float) decimals;
    }

    private String stringFormat(double doubleV) {
        int n2 = 0;
        StringTokenizer t=new StringTokenizer(String.valueOf(doubleV),".");
        String s1 = t.nextToken();
        String s2 = t.nextToken();
        n2 = s2.length();
        if(n2!=1) {
            if(s2.charAt(s2.length()-1)=='0') n2 = s2.length()-1;
        }
        if(n2 == 0 || (n2 == 1 && s2.charAt(0) == '0')) {
            return s1;
        }
        return doubleV + "";

    }

    @Nullable
    public PlayerGame getPlayerGame(final Player player) {
        return getPlayerGame(player.getUniqueId());
    }

    @Nullable
    public PlayerGame getPlayerGame(final UUID uuid) {
        for (final PlayerGame playerGame : this.players) {
            if (playerGame.getUniqueId().equals(uuid))
                return playerGame;
        }
        return null;
    }

    @Nullable
    public PlayerGame getOldPlayerGame(final UUID uuid) {
        for (final Squad squad : this.teams) {
            for (final PlayerGame playerGame : squad.getRemovedPlayers()) {
                if (playerGame.getUniqueId().equals(uuid))
                    return playerGame;
            }
        }
        return null;
    }

    public boolean isInvalid() {
        return this.circles.size() == 0 || this.plates.size() == 0 || this.spawn == null || this.returnLocation == null;
    }

    public static Map<Squad, Integer> shortSquads(final List<Squad> squads) {
        final Map<Squad, Integer> shortedSquads = new HashMap<>();
        int position = 1;
        while (squads.size() > 0) {
            final List<Squad> squad = new ArrayList<>();
            squad.add(squads.get(0));
            for (int i = 1; i < squads.size(); i++) {
                if (squad.get(0).getPoints() < squads.get(i).getPoints()) {
                    squad.clear();
                    squad.add(squads.get(i));
                    continue;
                }
                if (squad.get(0).getPoints() == squads.get(i).getPoints())
                    squad.add(squads.get(i));
            }
            for (final Squad shortedSquad : squad) {
                squads.remove(shortedSquad);
                shortedSquads.put(shortedSquad, position);
            }
            position++;
        }
        return shortedSquads;
    }

    public UUID getUuid() {
        return uuid;
    }

    public List<Circle> getCircles() {
        return circles;
    }

    public List<Plate> getPlates() {
        return plates;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTimer() {
        return timer;
    }

    public void setTimer(int timer) {
        this.timer = timer;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public void setTeamNumber(int teamNumber) {
        this.teamNumber = teamNumber;
    }

    public int getPointPerKill() {
        return pointPerKill;
    }

    public void setPointPerKill(int pointPerKill) {
        this.pointPerKill = pointPerKill;
    }

    public int getPointPerDeath() {
        return pointPerDeath;
    }

    public void setPointPerDeath(int pointPerDeath) {
        this.pointPerDeath = pointPerDeath;
    }

    public int getSecondsToCapture() {
        return secondsToCapture;
    }

    public void setSecondsToCapture(int secondsToCapture) {
        this.secondsToCapture = secondsToCapture;
    }

    public double getYenPerPoints() {
        return yenPerPoints;
    }

    public void setYenPerPoints(double yenPerPoints) {
        this.yenPerPoints = yenPerPoints;
    }

    public double getReward() {
        return reward;
    }

    public void setReward(double reward) {
        this.reward = reward;
    }

    public int getSpectateDuration() {
        return spectateDuration;
    }

    public void setSpectateDuration(int spectateDuration) {
        this.spectateDuration = spectateDuration;
    }

    public Location getSpawn() {
        return spawn;
    }

    public void setSpawn(Location spawn) {
        this.spawn = spawn;
    }

    public Location getReturnLocation() {
        return returnLocation;
    }

    public void setReturnLocation(Location returnLocation) {
        this.returnLocation = returnLocation;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public GameStatus getStatus() {
        return status;
    }

    public List<Squad> getTeams() {
        return teams;
    }

    public List<PlayerGame> getPlayers() {
        return players;
    }

    public int getDiscount() {
        return discount;
    }

    public double getMinPointsReward() {
        return minPointsReward;
    }

    public void setMinPointsReward(double minPointsReward) {
        this.minPointsReward = minPointsReward;
    }

    public double getMinParticipationReward() {
        return minParticipationReward;
    }

    public void setMinParticipationReward(double minParticipationReward) {
        this.minParticipationReward = minParticipationReward;
    }

}
