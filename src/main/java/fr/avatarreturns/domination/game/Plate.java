package fr.avatarreturns.domination.game;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector2;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import fr.avatarreturns.domination.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.jetbrains.annotations.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Plate {

    private final String name;
    private final String worldName;
    private transient double value;
    private transient Squad captured;
    private transient Squad capturing;

    public Plate(final String name, final String worldName) {
        this.name = name;
        this.worldName = worldName;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void addValue(double value) {
        this.value = this.value + value;
        if (this.value < 0)
            this.value = 0;
        else if (this.value > 1)
            this.value = 1;
    }

    public Squad getCaptured() {
        return captured;
    }

    public void setCaptured(Squad captured) {
        this.captured = captured;
    }

    public Squad getCapturing() {
        return capturing;
    }

    public void setCapturing(Squad capturing) {
        this.capturing = capturing;
    }

    public List<Squad> getSquadOn(final List<Squad> squads) {
        final List<Squad> squadsOn = new ArrayList<>();
        if (this.getRegion() == null)
            return squadsOn;
        final ProtectedRegion region = this.getRegion();
        for (final Squad squad : squads) {
            for (final PlayerGame playerGame : Utils.copy(squad.getPlayers())) {
                if (playerGame.getStatus() == null)
                    continue;
                if (playerGame.isOnline() && playerGame.getStatus().equals(PlayerType.PLAYING)) {
                    if (region.contains(getBlockVector2(playerGame.getPlayer().getLocation()))) {
                        squadsOn.add(squad);
                        break;
                    }
                }
            }
        }
        return squadsOn;
    }

    @Nullable
    private ProtectedRegion getRegion() {
        final RegionContainer regionContainer = WorldGuard.getInstance().getPlatform().getRegionContainer();
        final RegionManager regionManager = regionContainer.get(BukkitAdapter.adapt(Objects.requireNonNull(Bukkit.getWorld(this.worldName))));
        if (regionManager == null)
            return null;
        if (regionManager.getRegion(this.name) == null)
            return null;
        return regionManager.getRegion(this.name);
    }

    private BlockVector2 getBlockVector2(final Location location) {
        return BlockVector2.at(location.getBlockX(), location.getBlockZ());
    }
}
