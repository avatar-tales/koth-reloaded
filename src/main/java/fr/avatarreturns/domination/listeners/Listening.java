package fr.avatarreturns.domination.listeners;

import fr.avatarreturns.domination.game.*;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.Main;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.Objects;
import java.util.Optional;

@SuppressWarnings({"unused"})
public class Listening implements Listener {

    public Listening() {
        Main.getInstance().getServer().getPluginManager().registerEvents(this, Main.getInstance());
    }

    @EventHandler
    public void onPlayerDamage(final EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player))
            return;
        final Domination dominationPlayer = Domination.getDomination((Player) e.getEntity());
        if (dominationPlayer == null)
            return;
        if (dominationPlayer.getStatus().equals(GameStatus.WAITING))
            e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDeath(final PlayerDeathEvent e) {
        if (e.getEntity().getKiller() == null)
            return;
        final Domination dominationPlayer = Domination.getDomination(e.getEntity());
        final Domination dominationKiller = Domination.getDomination(e.getEntity().getKiller());
        if (dominationPlayer == null || dominationKiller == null)
            return;
        if (dominationPlayer != dominationKiller)
            return;
        e.setKeepInventory(true);
        e.setDroppedExp(0);
        e.getDrops().clear();
        final PlayerGame playerGame = dominationPlayer.getPlayerGame(e.getEntity());
        final PlayerGame killerGame = dominationPlayer.getPlayerGame(e.getEntity().getKiller());
        if (playerGame == null || killerGame == null)
            return;
        playerGame.addDeaths(1);
        killerGame.addKills(1);
        if (!Objects.equals(dominationPlayer.getPlayerSquad(killerGame), dominationPlayer.getPlayerSquad(playerGame))) {
            killerGame.addPoints(dominationKiller.getPointPerKill());
            killerGame.sendMessage(Lang.INFO_KILL.get(String.valueOf(dominationKiller.getPointPerKill())));
        } else {
            playerGame.sendMessage(Lang.WARNING_TEAMKILL_VICTIM.get(killerGame.getName()));
            killerGame.sendMessage(Lang.WARNING_TEAMKILL_ATTACKER.get(playerGame.getName()));
        }
        playerGame.addPoints(-dominationPlayer.getPointPerDeath());
        playerGame.sendMessage(Lang.INFO_DEATH.get(String.valueOf(dominationPlayer.getPointPerDeath()), String.valueOf(dominationPlayer.getSpectateDuration())));
        final Location location = e.getEntity().getLocation();
        Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
            playerGame.setStatus(PlayerType.SPECTATE);
            e.getEntity().spigot().respawn();
            Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
                e.getEntity().setGameMode(GameMode.SPECTATOR);
                e.getEntity().teleport(location);
                Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
                    playerGame.setStatus(PlayerType.PLAYING);
                    e.getEntity().teleport(dominationPlayer.getSpawn());
                    e.getEntity().setGameMode(GameMode.SURVIVAL);
                }, 20L * dominationPlayer.getSpectateDuration());
            }, 1L);
        }, 1L);
    }

    @EventHandler
    public void onPlayerDamage(final EntityDamageByEntityEvent e) {
        if (!(e.getEntity() instanceof Player) || !(e.getDamager() instanceof Player))
            return;
        final Domination dominationPlayer = Domination.getDomination((Player) e.getEntity());
        final Domination dominationKiller = Domination.getDomination((Player) e.getDamager());
        if (dominationPlayer == null || dominationKiller == null)
            return;
        if (dominationPlayer != dominationKiller)
            return;
        final PlayerGame playerGame = dominationPlayer.getPlayerGame((Player) e.getEntity());
        final PlayerGame killerGame = dominationPlayer.getPlayerGame((Player) e.getDamager());
        if (playerGame == null || killerGame == null)
            return;
        if (!dominationPlayer.getStatus().equals(GameStatus.GAME))
            return;
        if (Objects.equals(dominationPlayer.getPlayerSquad(killerGame), dominationPlayer.getPlayerSquad(playerGame)))
            killerGame.sendMessage(Lang.WARNING_TEAMDAMAGE.get(playerGame.getName()));
    }

    @EventHandler
    public void onTeleport(final PlayerTeleportEvent e) {
        if (e.getCause().equals(PlayerTeleportEvent.TeleportCause.SPECTATE) && Domination.getDomination(e.getPlayer()) != null) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent e) {
        final Optional<FallBackPlayer> toRemove = FallBackPlayer.getFallBackPlayers().parallelStream().filter(fallBackPlayer -> fallBackPlayer.getUniqueId().equals(e.getPlayer().getUniqueId())).findFirst();
        toRemove.ifPresent(fallBackPlayer -> {
            e.getPlayer().teleport(fallBackPlayer.getLocation());
            e.getPlayer().sendMessage(Lang.WARNING_FALLBACKPLAYER.get(fallBackPlayer.getTeamName()));
            FallBackPlayer.getFallBackPlayers().remove(fallBackPlayer);
        });
        Domination domination = null;
        for (final Domination dominations : Domination.getDominations()) {
            for (final Squad squad : dominations.getTeams()) {
                for (final PlayerGame playerGame : squad.getRemovedPlayers()) {
                    if (playerGame.getUniqueId().equals(e.getPlayer().getUniqueId()))
                        domination = dominations;
                }
            }
        }
        if (domination != null) {
            domination.addPlayer(e.getPlayer());
        }
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent e) {
        for (final Domination domination : Domination.getDominations()) {
            final PlayerGame playerGame = domination.getPlayerGame(e.getPlayer());
            if (playerGame == null)
                continue;
            domination.getPlayers().remove(playerGame);
            for (final Squad squad : domination.getTeams()) {
                if (!squad.getPlayers().contains(playerGame)) continue;
                squad.getPlayers().remove(playerGame);
                squad.getRemovedPlayers().add(playerGame);
            }
            playerGame.setBeFallBackPlayer(true);
            break;
        }
    }

    @EventHandler
    public void onInventoryInteract(final InventoryMoveItemEvent e) {
        if (e.getItem().getItemMeta() != null)
            if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Casque VR"))
                e.setCancelled(true);
    }

    @EventHandler
    public void onDrop(final PlayerDropItemEvent e) {
        if (e.getItemDrop().getItemStack().getItemMeta() != null)
            if (e.getItemDrop().getItemStack().getItemMeta().getDisplayName().equalsIgnoreCase("§6Casque VR"))
                e.setCancelled(true);
    }

    @EventHandler
    public void onPickup(final EntityPickupItemEvent e) {
        if (e.getItem().getItemStack().getItemMeta() != null)
            if (e.getItem().getItemStack().getItemMeta().getDisplayName().equalsIgnoreCase("§6Casque VR"))
                e.setCancelled(true);
    }


}
