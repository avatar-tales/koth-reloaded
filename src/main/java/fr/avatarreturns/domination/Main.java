package fr.avatarreturns.domination;

import fr.avatarreturns.domination.commands.CommandManager;
import fr.avatarreturns.domination.files.FileManager;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.game.FallBackPlayer;
import fr.avatarreturns.domination.game.GameStatus;
import fr.avatarreturns.domination.game.StopReason;
import fr.avatarreturns.domination.listeners.Listening;
import fr.avatarreturns.domination.manager.IManager;
import fr.avatarreturns.domination.manager.config.DefaultConfig;
import fr.avatarreturns.domination.manager.lang.LangManager;
import fr.avatarreturns.domination.manager.perms.PermissionsManager;
import fr.avatarreturns.domination.utils.Scheduler;
import fr.avatarreturns.domination.utils.Utils;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main extends JavaPlugin {

    private static Economy econ = null;

    private static Main instance;
    private Scheduler scheduler;

    private List<IManager> managers;

    public static Main getInstance() {
        return instance;
    }

    public static Economy getEcon() {
        return econ;
    }

    @Override
    public void onEnable() {
        super.onEnable();
        instance = this;
        /*if (!setupEconomy()) {
            getServer().getPluginManager().disablePlugin(this);
            System.out.println("NO ECONOMY");
            return;
        }*/
        managers = new ArrayList<>();
        managers.add(new PermissionsManager(this));
        managers.add(new LangManager(this));
        FileManager.init();
        int success = 0;
        for (final File file : FileManager.getSaveDir().listFiles()) {
            try {
                final Domination domination = FileManager.load(file);
                if (domination == null)
                    continue;
                domination.init();
                Domination.getDominations().add(domination);
                success++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (final File file : Utils.copy(FileManager.getFallBackDir().listFiles())) {
            try {
                final FallBackPlayer fallBackPlayer = FileManager.loadFallBack(file);
                assert fallBackPlayer != null;
                FallBackPlayer.getFallBackPlayers().add(fallBackPlayer);
                FileManager.deleteFile(file.getAbsolutePath(), file.getName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        DefaultConfig.init(this.getDataFolder().getAbsolutePath());
        new CommandManager();
        new Listening();
        this.scheduler = new Scheduler();
        this.scheduler.start();
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "bungee:bot");
    }

    @Override
    public void onDisable() {
        this.scheduler.stop();
        for (final Domination domination : Domination.getDominations()) {
            try {
                if (domination.getStatus().equals(GameStatus.GAME))
                    domination.stop(StopReason.COMMAND);
                FileManager.save(domination);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (final FallBackPlayer fallBackPlayer : FallBackPlayer.getFallBackPlayers()) {
            try {
                FileManager.save(fallBackPlayer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public List<IManager> getManagers() {
        return managers;
    }
}
