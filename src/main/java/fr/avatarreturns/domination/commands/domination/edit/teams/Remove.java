package fr.avatarreturns.domination.commands.domination.edit.teams;

import fr.avatarreturns.domination.commands.IEditCommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.game.FallBackPlayer;
import fr.avatarreturns.domination.game.PlayerGame;
import fr.avatarreturns.domination.game.Squad;
import fr.avatarreturns.domination.utils.Utils;
import org.bukkit.command.CommandSender;

import java.util.List;

public class Remove implements IEditCommand {

    @Override
    public boolean run(Domination domination, CommandSender commandSender, List<String> args) {
        Squad looked = null;
        for (final Squad squad : domination.getTeams()) {
            if (squad.getAbsoluteName().equalsIgnoreCase(args.get(0))) {
                looked = squad;
                break;
            }
        }
        if (looked == null) {
            return false;
        }
        domination.getTeams().remove(looked);
        for (final PlayerGame playerGame : Utils.copy(looked.getPlayers())) {
            looked.removePlayer(playerGame);
            domination.getMinSquad().addPlayer(playerGame);
        }
        for (final PlayerGame playerGame : Utils.copy(looked.getRemovedPlayers())) {
            if (playerGame.canBeFallBackPlayer())
                new FallBackPlayer(playerGame.getUniqueId(), domination.getReturnLocation());
        }
        return true;
    }

}
