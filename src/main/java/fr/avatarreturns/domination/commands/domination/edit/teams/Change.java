package fr.avatarreturns.domination.commands.domination.edit.teams;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.IEditCommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.game.PlayerGame;
import fr.avatarreturns.domination.game.Squad;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.command.CommandSender;

import java.util.Iterator;
import java.util.List;

public class Change implements IEditCommand {

    @CommandAnnotations(command = {"change"}, usage = "domination edit <name> team change <player> <name>", permission = Permissions.EDIT_TEAMS)
    @Override
    public boolean run(Domination domination, CommandSender commandSender, List<String> args) {
        final PlayerGame pp = domination.getPlayerGame(Bukkit.getPlayer(args.get(0)));
        domination.getPlayerSquad(pp).removePlayer(pp);
        for (final Squad squad : domination.getTeams()) {
            if (squad.getAbsoluteName().equalsIgnoreCase(args.get(1)))
                squad.addPlayer(pp);
        }
        return true;
    }
}
