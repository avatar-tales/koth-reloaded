package fr.avatarreturns.domination.commands.domination;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;

public class List implements ICommand {

    @CommandAnnotations(command = {"list"}, usage = "domination list", permission = Permissions.LIST)
    @Override
    public boolean run(CommandSender commandSender, java.util.List<String> args) {
        if (Domination.getDominations().size() == 0) {
            commandSender.sendMessage(Lang.ERROR_EMPTY_LIST.get());
            return true;
        }
        final StringBuilder stringBuilder = new StringBuilder();
        for (final Domination domination : Domination.getDominations()) {
            stringBuilder.append("&6").append(domination.getName());
            if (!domination.equals(Domination.getDominations().get(Domination.getDominations().size() - 1))) {
                stringBuilder.append("&7, ");
            }
        }
        commandSender.sendMessage(Lang.INFO_LIST.get(stringBuilder.toString()));
        return true;
    }

}
