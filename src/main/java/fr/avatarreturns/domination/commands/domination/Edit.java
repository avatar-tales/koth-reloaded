package fr.avatarreturns.domination.commands.domination;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICommand;
import fr.avatarreturns.domination.commands.IEditCommand;
import fr.avatarreturns.domination.commands.ISimpleCommand;
import fr.avatarreturns.domination.commands.domination.edit.circle.CircleCommandManager;
import fr.avatarreturns.domination.commands.domination.edit.other.*;
import fr.avatarreturns.domination.commands.domination.edit.plate.PlateCommandManager;
import fr.avatarreturns.domination.commands.domination.edit.teams.TeamCommandManager;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.manager.lang.Lang;
import org.bukkit.command.CommandSender;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class Edit implements ICommand {

    public static List<ISimpleCommand> commandHandler;

    public Edit() {
        commandHandler = new ArrayList<>();
        commandHandler.add(new CircleCommandManager());
        commandHandler.add(new PlateCommandManager());
        commandHandler.add(new TeamCommandManager());
        commandHandler.add(new ChannelId());
        commandHandler.add(new PointsPerDeath());
        commandHandler.add(new PointsPerKill());
        commandHandler.add(new MinParticipationReward());
        commandHandler.add(new MinPointsReward());
        commandHandler.add(new Return());
        commandHandler.add(new SecondsToCapture());
        commandHandler.add(new Spawn());
        commandHandler.add(new SpectateDuration());
        commandHandler.add(new TeamNumber());
        commandHandler.add(new Timer());
        commandHandler.add(new YenPerPoints());
    }

    @CommandAnnotations(command = {"edit"}, usage = "domination edit <name>", showInHelp = false)
    @Override
    public boolean run(CommandSender commandSender, List<String> args) {
        final Domination domination = Domination.getDomination(args.get(0));
        if (domination == null) {
            commandSender.sendMessage(Lang.ERROR_USAGE.get("domination edit <name>"));
            return false;
        }
        for (final ISimpleCommand commandCore : commandHandler) {
            for (final Method method : commandCore.getClass().getDeclaredMethods()) {
                final CommandAnnotations annotationCommand = method.getDeclaredAnnotation(CommandAnnotations.class);
                if (annotationCommand == null) continue;
                for (final String cmds : annotationCommand.command()) {
                    if (cmds.equalsIgnoreCase(args.get(1))) {
                        if (annotationCommand.permission().hasPermission(commandSender)) {
                            List<String> arguments = new ArrayList<>();
                            int i = -1;
                            for (final String arg : args) {
                                i++;
                                if (i <= 1) continue;
                                arguments.add(arg);
                            }
                            try {
                                return (boolean) method.invoke(commandCore, domination, commandSender, arguments);
                            } catch (Exception e) {
                                commandSender.sendMessage(Lang.ERROR_USAGE.get(annotationCommand.usage()));
                                e.printStackTrace();
                                return false;
                            }
                        } else {
                            commandSender.sendMessage(Lang.ERROR_PERMISSION.get());
                        }
                    }
                }
            }
        }
        return false;
    }

}
