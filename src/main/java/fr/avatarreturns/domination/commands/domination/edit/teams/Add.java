package fr.avatarreturns.domination.commands.domination.edit.teams;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.IEditCommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.game.Squad;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.Color;
import org.bukkit.command.CommandSender;

import java.util.List;

public class Add implements IEditCommand {

    @CommandAnnotations(command = {"add"}, usage = "domination edit <name> team add <name> <color : r,g,b>", permission = Permissions.EDIT_TEAMS)
    @Override
    public boolean run(Domination domination, CommandSender commandSender, List<String> args) {
        for (final Squad squad : domination.getTeams()) {
            if (squad.getAbsoluteName().equalsIgnoreCase(args.get(0))) {
                return false;
            }
        }
        Color color = null;
        try {
            color = Color.fromRGB(Integer.parseInt(args.get(1)), Integer.parseInt(args.get(2)), Integer.parseInt(args.get(3)));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        domination.getTeams().add(new Squad(domination, args.get(0), color));
        return true;
    }

}
