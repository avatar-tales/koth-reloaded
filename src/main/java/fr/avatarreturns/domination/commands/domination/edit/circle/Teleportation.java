package fr.avatarreturns.domination.commands.domination.edit.circle;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICircleCommand;
import fr.avatarreturns.domination.game.Circle;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class Teleportation implements ICircleCommand {

    @CommandAnnotations(command = {"teleportation"}, usage = "domination edit <name> circle <name> teleportation <location e.g. : (world,x,y,z,yaw,pitch | here)>", permission = Permissions.EDIT_CIRCLE)
    @Override
    public boolean run(Circle circle, CommandSender commandSender, List<String> args) {
        if (args.get(0).equalsIgnoreCase("here")) {
            if (commandSender instanceof Player) {
                circle.setTeleport(((Player) commandSender).getLocation());
                commandSender.sendMessage(Lang.INFO_EDIT_CIRCLE_UPDATE.get());
                return true;
            } else {
                commandSender.sendMessage(Lang.ERROR_NOT_PLAYER.get());
                return false;
            }
        }
        final String[] locationPart = args.get(0).split(",");
        final String world = locationPart[0];
        final double x = Double.parseDouble(locationPart[1]);
        final double y = Double.parseDouble(locationPart[2]);
        final double z = Double.parseDouble(locationPart[3]);
        final float yaw = Float.parseFloat(locationPart[4]);
        final float pitch = Float.parseFloat(locationPart[5]);
        final org.bukkit.Location location = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
        circle.setTeleport(location);
        commandSender.sendMessage(Lang.INFO_EDIT_CIRCLE_UPDATE.get());
        return true;
    }
}
