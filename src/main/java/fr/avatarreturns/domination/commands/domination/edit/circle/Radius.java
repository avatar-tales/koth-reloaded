package fr.avatarreturns.domination.commands.domination.edit.circle;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICircleCommand;
import fr.avatarreturns.domination.game.Circle;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;

import java.util.List;

public class Radius implements ICircleCommand {

    @CommandAnnotations(command = {"radius"}, usage = "domination edit <name> circle <name> radius <float>", permission = Permissions.EDIT_CIRCLE)
    @Override
    public boolean run(Circle circle, CommandSender commandSender, List<String> args) {
        circle.setRadius(Float.parseFloat(args.get(0)));
        commandSender.sendMessage(Lang.INFO_EDIT_CIRCLE_UPDATE.get());
        return true;
    }

}
