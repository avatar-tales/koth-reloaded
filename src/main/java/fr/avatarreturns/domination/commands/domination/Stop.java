package fr.avatarreturns.domination.commands.domination;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.game.GameStatus;
import fr.avatarreturns.domination.game.StopReason;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;

public class Stop implements ICommand {

    @CommandAnnotations(command = {"stop"}, usage = "domination stop <name>", permission = Permissions.START)
    @Override
    public boolean run(CommandSender commandSender, java.util.List<String> args) {
        final Domination domination = Domination.getDomination(args.get(0));
        if (domination == null) {
            return false;
        }
        if (!domination.getStatus().equals(GameStatus.GAME))
            return false;
        domination.stop(StopReason.COMMAND);
        return true;
    }
}
