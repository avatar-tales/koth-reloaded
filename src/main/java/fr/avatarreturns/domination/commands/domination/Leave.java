package fr.avatarreturns.domination.commands.domination;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class Leave implements ICommand {

    @CommandAnnotations(command = "leave", usage = "domination leave", permission = Permissions.LEAVE)
    @Override
    public boolean run(CommandSender commandSender, List<String> args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(Lang.ERROR_NOT_PLAYER.get());
            return false;
        }
        final Domination domination = Domination.getDomination((Player) commandSender);
        if (domination == null) {
            commandSender.sendMessage(Lang.ERROR_NOT_IN_GAME.get());
            return false;
        }
        return domination.removePlayer((Player) commandSender);
    }

}
