package fr.avatarreturns.domination.commands.domination;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.CommandManager;
import fr.avatarreturns.domination.commands.ICommand;
import fr.avatarreturns.domination.commands.domination.edit.circle.CircleCommandManager;
import fr.avatarreturns.domination.commands.domination.edit.plate.PlateCommandManager;
import fr.avatarreturns.domination.commands.domination.edit.teams.TeamCommandManager;
import org.bukkit.command.CommandSender;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Help implements ICommand {

    @CommandAnnotations(command = {"help"})
    @Override
    public boolean run(CommandSender commandSender, List<String> args) {
        final List<Method> methods = new ArrayList<>();
        final List<Object> objects = new ArrayList<>();
        objects.addAll(CommandManager.commandHandler);
        objects.addAll(Edit.commandHandler);
        objects.addAll(PlateCommandManager.commandHandler);
        objects.addAll(CircleCommandManager.commandHandler);
        objects.addAll(TeamCommandManager.commandHandler);
        for (final Object o : objects) {
            methods.addAll(Arrays.asList(o.getClass().getDeclaredMethods()));
        }
        for (final Method method : methods) {
            final CommandAnnotations annotationCommand = method.getDeclaredAnnotation(CommandAnnotations.class);
            if (annotationCommand == null) continue;
            if (!annotationCommand.showInHelp()) continue;
            commandSender.sendMessage("§6§l-|> §7" + annotationCommand.usage());
        }

        return true;
    }

}
