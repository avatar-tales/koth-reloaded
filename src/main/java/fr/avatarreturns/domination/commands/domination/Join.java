package fr.avatarreturns.domination.commands.domination;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class Join implements ICommand {

    @CommandAnnotations(command = "join", usage = "domination join <nom> [<team>]", permission = Permissions.JOIN)
    @Override
    public boolean run(CommandSender commandSender, List<String> args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(Lang.ERROR_NOT_PLAYER.get());
            return false;
        }
        if (Domination.getDomination((Player) commandSender) != null) {
            commandSender.sendMessage(Lang.ERROR_IN_GAME.get());
            return false;
        }
        final Domination domination = Domination.getDomination(args.get(0));
        if (domination == null) {
            commandSender.sendMessage(Lang.ERROR_INEXIST.get());
            return false;
        }
        if (args.size() >= 2)
            return domination.addPlayer((Player) commandSender, args.get(1));
        return domination.addPlayer((Player) commandSender);
    }

}
