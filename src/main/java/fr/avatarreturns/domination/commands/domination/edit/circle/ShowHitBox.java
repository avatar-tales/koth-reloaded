package fr.avatarreturns.domination.commands.domination.edit.circle;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICircleCommand;
import fr.avatarreturns.domination.game.Circle;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;

import java.util.List;

public class ShowHitBox implements ICircleCommand {

    @CommandAnnotations(command = {"showhitbox"}, usage = "domination edit <name> circle <name> showhitbox <boolean>", permission = Permissions.EDIT_CIRCLE)
    @Override
    public boolean run(Circle circle, CommandSender commandSender, List<String> args) {
        circle.setShowHitBox(Boolean.parseBoolean(args.get(0)));
        commandSender.sendMessage(Lang.INFO_EDIT_CIRCLE_UPDATE.get());
        return true;
    }

}
