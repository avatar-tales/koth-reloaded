package fr.avatarreturns.domination.commands.domination;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;

import java.util.List;

public class Create implements ICommand {

    @CommandAnnotations(command = "create", usage = "domination create <nom>", permission = Permissions.CREATE)
    @Override
    public boolean run(CommandSender commandSender, List<String> args) {
        if (Domination.getDomination(args.get(0)) != null) {
            commandSender.sendMessage(Lang.ERROR_EXIST.get());
            return false;
        }
        final Domination domination = new Domination(args.get(0));
        Domination.getDominations().add(domination);
        commandSender.sendMessage(Lang.INFO_CREATE.get(args.get(0)));
        return true;
    }
}
