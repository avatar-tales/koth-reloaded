package fr.avatarreturns.domination.commands.domination.edit.other;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.IEditCommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;

import java.util.List;

public class SecondsToCapture implements IEditCommand {

    @CommandAnnotations(command = {"secondsToCapture"}, usage = "domination edit <name> secondsToCapture <integer>", permission = Permissions.EDIT_OTHER)
    @Override
    public boolean run(Domination domination, CommandSender commandSender, List<String> args) {
        domination.setSecondsToCapture(Integer.parseInt(args.get(0)));
        commandSender.sendMessage(Lang.INFO_EDIT_UPDATE.get());
        return true;
    }
}
