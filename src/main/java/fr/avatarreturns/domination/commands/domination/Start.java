package fr.avatarreturns.domination.commands.domination;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.game.GameStatus;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;

public class Start implements ICommand {

    @CommandAnnotations(command = {"start"}, usage = "domination start <name>", permission = Permissions.START)
    @Override
    public boolean run(CommandSender commandSender, java.util.List<String> args) {
        final Domination domination = Domination.getDomination(args.get(0));
        if (domination == null) {
            commandSender.sendMessage(Lang.ERROR_INEXIST.get());
            return false;
        }
        if (domination.getStatus().equals(GameStatus.GAME)) {
            commandSender.sendMessage(Lang.ERROR_START.get());
            return false;
        }
        try {
            domination.start();
            commandSender.sendMessage(Lang.INFO_STARTER.get());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
