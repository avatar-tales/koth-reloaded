package fr.avatarreturns.domination.commands.domination.edit.circle;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.IEditCommand;
import fr.avatarreturns.domination.game.Circle;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class Add implements IEditCommand {

    @CommandAnnotations(command = {"add"}, usage = "domination edit <name> circle add <name> <location e.g. : world,x,y,z,yaw,pitch>", permission = Permissions.EDIT_CIRCLE)
    @Override
    public boolean run(final Domination domination, final CommandSender commandSender, final List<String> args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(Lang.ERROR_NOT_PLAYER.get());
            return false;
        }
        final String name = args.get(0);
        for (final Circle circles : domination.getCircles()) {
            if (circles.getId().equalsIgnoreCase(name)) {
                commandSender.sendMessage(Lang.ERROR_EDIT_CIRCLE_FOUND.get());
                return false;
            }
        }
        final String[] locationPart = args.get(1).split(",");
        final String world = locationPart[0];
        final double x = Double.parseDouble(locationPart[1]);
        final double y = Double.parseDouble(locationPart[2]);
        final double z = Double.parseDouble(locationPart[3]);
        final float yaw = Float.parseFloat(locationPart[4]);
        final float pitch = Float.parseFloat(locationPart[5]);
        final Location location = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
        domination.getCircles().add(new Circle(name, ((Player) commandSender).getLocation(), location, 2.5, Color.SILVER, 0.75, false));
        commandSender.sendMessage(Lang.INFO_EDIT_CIRCLE_CREATE.get(domination.getName()));
        return true;
    }
}
