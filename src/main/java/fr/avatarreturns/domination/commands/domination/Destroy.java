package fr.avatarreturns.domination.commands.domination;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;

public class Destroy implements ICommand {

    @CommandAnnotations(command = {"destroy"}, usage = "domination destroy <name>", permission = Permissions.DESTROY)
    @Override
    public boolean run(CommandSender commandSender, java.util.List<String> args) {
        if (Domination.getDomination(args.get(0)) == null) {
            commandSender.sendMessage(Lang.ERROR_INEXIST.get());
            return false;
        }
        try {
            Domination.getDominations().remove(Domination.getDomination(args.get(0)));
            commandSender.sendMessage(Lang.INFO_DESTROY.get());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
