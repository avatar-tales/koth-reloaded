package fr.avatarreturns.domination.commands.domination.edit.circle;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICircleCommand;
import fr.avatarreturns.domination.game.Circle;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;

import java.util.List;

public class Color implements ICircleCommand {

    @CommandAnnotations(command = {"color"}, usage = "domination edit <name> circle <name> color <color e.g. : red,green,blue>", permission = Permissions.EDIT_CIRCLE)
    @Override
    public boolean run(Circle circle, CommandSender commandSender, List<String> args) {
        final String[] colorPart = args.get(0).split(",");
        final int r = Integer.parseInt(colorPart[0]);
        final int g = Integer.parseInt(colorPart[1]);
        final int b = Integer.parseInt(colorPart[2]);
        final org.bukkit.Color color = org.bukkit.Color.fromRGB(r, g, b);
        circle.setColor(color);
        commandSender.sendMessage(Lang.INFO_EDIT_CIRCLE_UPDATE.get());
        return true;
    }

}
