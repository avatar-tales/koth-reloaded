package fr.avatarreturns.domination.commands.domination.edit.circle;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICircleCommand;
import fr.avatarreturns.domination.commands.IEditCommand;
import fr.avatarreturns.domination.commands.ISimpleCommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.manager.lang.Lang;
import org.bukkit.command.CommandSender;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class CircleCommandManager implements IEditCommand {

    public static List<ISimpleCommand> commandHandler;

    public CircleCommandManager() {
        commandHandler = new ArrayList<>();
        commandHandler.add(new Add());
        commandHandler.add(new Remove());
        commandHandler.add(new Color());
        commandHandler.add(new Depth());
        commandHandler.add(new Location());
        commandHandler.add(new Radius());
        commandHandler.add(new ShowHitBox());
        commandHandler.add(new Teleportation());
    }

    @CommandAnnotations(command = {"circle"}, usage = "domination edit <name> circle", showInHelp = false)
    @Override
    public boolean run(final Domination domination, final CommandSender commandSender, final List<String> args) {
        if (args.size() == 0) {
            return false;
        }
        for (final ISimpleCommand commandCore : commandHandler) {
            for (final Method method : commandCore.getClass().getDeclaredMethods()) {
                final CommandAnnotations annotationCommand = method.getDeclaredAnnotation(CommandAnnotations.class);
                if (annotationCommand == null) continue;
                for (final String cmds : annotationCommand.command()) {
                    if (cmds.equalsIgnoreCase(args.get(0))) {
                        if (annotationCommand.permission().hasPermission(commandSender)) {
                            List<String> arguments = new ArrayList<>();
                            int i = -1;
                            for (final String arg : args) {
                                i++;
                                if (i == 0) continue;
                                arguments.add(arg);
                            }
                            try {
                                return (boolean) method.invoke(commandCore, domination, commandSender, arguments);
                            } catch (Exception e) {
                                commandSender.sendMessage(Lang.ERROR_USAGE.get(annotationCommand.usage()));
                                e.printStackTrace();
                                return false;
                            }
                        } else {
                            commandSender.sendMessage(Lang.ERROR_PERMISSION.get());
                        }
                    }
                }
            }
        }
        return false;
    }
}
