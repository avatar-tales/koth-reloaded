package fr.avatarreturns.domination.commands.domination.edit.plate;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.IEditCommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.game.Plate;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class Remove implements IEditCommand {

    @CommandAnnotations(command = {"remove"}, usage = "domination edit <name> plate remove <name>", permission = Permissions.EDIT_CIRCLE)
    @Override
    public boolean run(final Domination domination, final CommandSender commandSender, final List<String> args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(Lang.ERROR_NOT_PLAYER.get());
            return false;
        }
        final String name = args.get(0);
        Plate plate = null;
        for (final Plate plates : domination.getPlates()) {
            if (plates.getName().equalsIgnoreCase(name)) {
                plate = plates;
                break;
            }
        }
        if (plate == null) {
            commandSender.sendMessage(Lang.ERROR_EDIT_PLATE_NOTFOUND.get());
            return false;
        }
        domination.getPlates().remove(plate);
        commandSender.sendMessage(Lang.INFO_EDIT_PLATE_REMOVE.get(domination.getName()));
        return false;
    }
}
