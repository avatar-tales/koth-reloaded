package fr.avatarreturns.domination.commands.domination;

import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.ICommand;
import fr.avatarreturns.domination.files.FileManager;
import fr.avatarreturns.domination.game.Domination;
import org.bukkit.command.CommandSender;

import java.io.IOException;

public class Save implements ICommand {

    @CommandAnnotations(command = {"save"}, usage = "domination save <name | all>")
    @Override
    public boolean run(CommandSender commandSender, java.util.List<String> args) {
        final String name = args.get(0);
        if (name.equalsIgnoreCase("all")) {
            for (final Domination domination : Domination.getDominations()) {
                try {
                    FileManager.save(domination);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            for (final Domination domination : Domination.getDominations()) {
                try {
                    if (domination.getName().equalsIgnoreCase(name))
                        FileManager.save(domination);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }
}
