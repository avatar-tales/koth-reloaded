package fr.avatarreturns.domination.commands.domination.edit.plate;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import fr.avatarreturns.domination.commands.CommandAnnotations;
import fr.avatarreturns.domination.commands.IEditCommand;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.game.Plate;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.manager.perms.Permissions;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class Add implements IEditCommand {

    @CommandAnnotations(command = {"add"}, usage = "domination edit <name> plate add <name>", permission = Permissions.EDIT_PLATE)
    @Override
    public boolean run(final Domination domination, final CommandSender commandSender, final List<String> args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(Lang.ERROR_NOT_PLAYER.get());
            return false;
        }
        final String name = args.get(0);
        for (final Plate plate : domination.getPlates()) {
            if (plate.getName().equalsIgnoreCase(name)) {
                commandSender.sendMessage(Lang.ERROR_EDIT_PLATE_ADD.get());
                return false;
            }
        }
        final RegionContainer regionContainer = WorldGuard.getInstance().getPlatform().getRegionContainer();
        final RegionManager regionManager = regionContainer.get(BukkitAdapter.adapt(((Player) commandSender).getWorld()));
        assert regionManager != null;
        for (final ProtectedRegion protectedRegion : regionManager.getRegions().values()) {
            if (!protectedRegion.getId().equalsIgnoreCase(name)) continue;
            domination.getPlates().add(new Plate(protectedRegion.getId(), ((Player) commandSender).getWorld().getName()));
            commandSender.sendMessage(Lang.INFO_EDIT_PLATE_ADD.get(domination.getName()));
            return true;
        }
        commandSender.sendMessage(Lang.ERROR_EDIT_PLATE_REGION_NOTFOUND.get());
        return false;
    }
}
