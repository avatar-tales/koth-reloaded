package fr.avatarreturns.domination.commands;

import fr.avatarreturns.domination.game.Circle;
import org.bukkit.command.CommandSender;

import java.util.List;

public interface ICircleCommand extends ISimpleCommand {

    boolean run(final Circle circle, final CommandSender commandSender, final List<String> args);

}
