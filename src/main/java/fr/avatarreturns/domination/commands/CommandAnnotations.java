package fr.avatarreturns.domination.commands;

import fr.avatarreturns.domination.manager.perms.Permissions;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface CommandAnnotations {

    String[] command();

    String usage() default "domination help";

    Permissions permission() default Permissions.NONE;

    boolean showInHelp() default true;

}
