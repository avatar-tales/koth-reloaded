package fr.avatarreturns.domination.commands;

import org.bukkit.command.CommandSender;

import java.util.List;

public interface ICommand extends ISimpleCommand {

    boolean run(final CommandSender commandSender, final List<String> args);

}
