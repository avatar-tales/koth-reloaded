package fr.avatarreturns.domination.commands;

import fr.avatarreturns.domination.commands.domination.*;
import fr.avatarreturns.domination.manager.lang.Lang;
import fr.avatarreturns.domination.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class CommandManager implements CommandExecutor {

    public static List<ISimpleCommand> commandHandler;

    public CommandManager() {
        commandHandler = new ArrayList<>();
        commandHandler.add(new Create());
        commandHandler.add(new Destroy());
        commandHandler.add(new Edit());
        commandHandler.add(new Help());
        commandHandler.add(new Join());
        commandHandler.add(new Leave());
        commandHandler.add(new fr.avatarreturns.domination.commands.domination.List());
        commandHandler.add(new Save());
        commandHandler.add(new Start());
        commandHandler.add(new Stop());
        for (final String command : Main.getInstance().getDescription().getCommands().keySet()) {
            try {
                assert Main.getInstance().getCommand(command) != null;
                Main.getInstance().getCommand(command).setExecutor(this);
            } catch (Exception ignored) {

            }
        }
    }


    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length == 0) {
            return false;
        }
        for (final ISimpleCommand commandCore : commandHandler) {
            for (final Method method : commandCore.getClass().getDeclaredMethods()) {
                final CommandAnnotations annotationCommand = method.getDeclaredAnnotation(CommandAnnotations.class);
                if (annotationCommand == null) continue;
                for (final String cmds : annotationCommand.command()) {
                    if (cmds.equalsIgnoreCase(args[0])) {
                        if (annotationCommand.permission().hasPermission(commandSender)) {
                            List<String> arguments = new ArrayList<>();
                            int i = -1;
                            for (final String arg : args) {
                                i++;
                                if (i == 0) continue;
                                arguments.add(arg);
                            }
                            try {
                                return (boolean) method.invoke(commandCore, commandSender, arguments);
                            } catch (Exception e) {
                                commandSender.sendMessage(Lang.ERROR_USAGE.get(annotationCommand.usage()));
                                e.printStackTrace();
                                return false;
                            }
                        } else {
                            commandSender.sendMessage(Lang.ERROR_PERMISSION.get());
                        }
                    }
                }
            }
        }
        return false;
    }

}
