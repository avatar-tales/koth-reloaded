package fr.avatarreturns.domination.commands;

import fr.avatarreturns.domination.game.Domination;
import org.bukkit.command.CommandSender;

import java.util.List;

public interface IEditCommand extends ISimpleCommand {

    boolean run(final Domination domination, final CommandSender commandSender, final List<String> args);

}
