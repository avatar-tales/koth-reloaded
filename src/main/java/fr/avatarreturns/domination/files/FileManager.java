package fr.avatarreturns.domination.files;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.game.FallBackPlayer;
import fr.avatarreturns.domination.Main;
import fr.avatarreturns.domination.utils.ColorJSON;
import fr.avatarreturns.domination.utils.LocationJSON;
import org.bukkit.Color;
import org.bukkit.Location;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.UUID;

@SuppressWarnings({"unused"})
public class FileManager {

    private static File saveDir;
    private static File fallBackDir;
    private static Gson gson;

    public static File getSaveDir() {
        return saveDir;
    }

    public static File getFallBackDir() {
        return fallBackDir;
    }

    public static void init() {
        saveDir = new File(Main.getInstance().getDataFolder(), "/instances/");
        if (!saveDir.exists()) {
            saveDir.mkdirs();
        }
        fallBackDir = new File(Main.getInstance().getDataFolder(), "/fallbackplayers/");
        if (!fallBackDir.exists()) {
            fallBackDir.mkdirs();
        }
        final Type typeLocation = new TypeToken<Location>() {
        }.getType();
        final Type typeColor = new TypeToken<Color>() {
        }.getType();
        gson = new GsonBuilder()
                .registerTypeAdapter(typeLocation, new LocationJSON())
                .registerTypeAdapter(typeColor, new ColorJSON())
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .serializeNulls()
                .create();
    }

    public static void save(final @NotNull Domination game) throws IOException {
        final File file = new File(saveDir, game.getUniqueId().toString() + ".json");
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
        final FileWriter fileWriter = new FileWriter(file);
        final String json = gson.toJson(game);
        fileWriter.write(json);
        fileWriter.flush();
        fileWriter.close();
    }

    public static void save(final @NotNull FallBackPlayer fallBackPlayer) throws IOException {
        final File file = new File(fallBackDir, fallBackPlayer.getUniqueId().toString() + ".json");
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
        final FileWriter fileWriter = new FileWriter(file);
        final String json = gson.toJson(fallBackPlayer);
        fileWriter.write(json);
        fileWriter.flush();
        fileWriter.close();
    }

    public static void remove(final @NotNull Domination game) throws IOException {
        final File file = new File(saveDir, game.getUniqueId().toString() + ".json");
        if (!file.exists()) {
            return;
        }
        final FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("");
        fileWriter.flush();
        fileWriter.close();
        deleteFile(saveDir.getAbsolutePath(), game.getUniqueId().toString() + ".json");
    }

    public static void remove(final @NotNull FallBackPlayer fallBackPlayer) throws IOException {
        final File file = new File(fallBackDir, fallBackPlayer.getUniqueId().toString() + ".json");
        if (!file.exists()) {
            return;
        }
        final FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("");
        fileWriter.flush();
        fileWriter.close();
        deleteFile(fallBackDir.getAbsolutePath(), fallBackPlayer.getUniqueId().toString() + ".json");
    }

    public static @Nullable Domination load(final String id) throws IOException {
        UUID uuid;
        try {
            uuid = UUID.fromString(id);
        } catch (Exception ignored) {
            return null;
        }
        return load(uuid);
    }

    public static Domination load(final @NotNull UUID uuid) throws IOException {
        final File file = new File(saveDir, uuid.toString() + ".json");
        return load(file);
    }

    public static @Nullable Domination load(final @NotNull File file) throws IOException {
        if (!file.exists()) {
            return null;
        }
        final BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        try {
            final StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            bufferedReader.close();
            return gson.fromJson(stringBuilder.toString(), Domination.class);
        } catch (IOException e) {
            e.printStackTrace();
            bufferedReader.close();
        }
        return null;
    }

    public static @Nullable FallBackPlayer loadFallBack(final @NotNull File file) throws IOException {
        if (!file.exists()) {
            return null;
        }
        final BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        try {
            final StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            bufferedReader.close();
            return gson.fromJson(stringBuilder.toString(), FallBackPlayer.class);
        } catch (IOException e) {
            e.printStackTrace();
            bufferedReader.close();
        }
        return null;
    }

    public static void deleteFile(final String path, final String name) throws IOException {
        java.nio.file.Files.walk(Paths.get(path))
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .filter(file -> file.getName().equalsIgnoreCase(name))
                .forEach(File::delete);
    }

}
