package fr.avatarreturns.domination.utils;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static <T> List<T> copy(final List<T> data) {
        final List<T> copied = new ArrayList<>();
        for (final T t : data) {
            final boolean add = copied.add(t);
        }
        return copied;
    }

    public static <T> List<T> copy(final T[] data) {
        final List<T> copied = new ArrayList<>();
        for (final T t : data) {
            final boolean add = copied.add(t);
        }
        return copied;
    }

}
