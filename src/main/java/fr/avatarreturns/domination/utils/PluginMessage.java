package fr.avatarreturns.domination.utils;

import fr.avatarreturns.domination.game.Domination;
import fr.avatarreturns.domination.game.PlayerGame;
import fr.avatarreturns.domination.game.Squad;
import fr.avatarreturns.domination.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PluginMessage {

    public static void sendResult(final String channelId, final String name, final List<Squad> squads) {
        final Map<Squad, Integer> shortedSquads = Domination.shortSquads(Utils.copy(squads));
        final List<String> messages = new ArrayList<>();
        messages.add("~~===================================~~");
        messages.add(":crossed_swords: Résultats de la partie " + name);
        final List<Squad> winners = new ArrayList<>();
        if (squads.size() == 0)
            return;
        winners.add(squads.get(0));
        for (int i = 1; i < squads.size(); i++) {
            if (winners.get(0).getPoints() == squads.get(i).getPoints()) {
                winners.add(squads.get(i));
                continue;
            }
            if (winners.get(0).getPoints() < squads.get(i).getPoints()) {
                winners.clear();
                winners.add(squads.get(i));
            }
        }
        final StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < winners.size(); i++) {
            stringBuilder.append(winners.get(i).getAbsoluteName()).append(" (").append(winners.get(i).getPoints()).append(")");
            if (i < winners.size() - 1)
                stringBuilder.append(", ");
        }
        messages.add("> Victoire de ou des équipes " + stringBuilder.toString());
        for (final Squad squad : squads) {
            String position = "";
            switch (shortedSquads.get(squad)) {
                case 1: {
                    position = ":first_place: ";
                    break;
                }
                case 2: {
                    position = ":second_place: ";
                    break;
                }
                case 3: {
                    position = ":third_place: ";
                    break;
                }
            }
            messages.add("> [∫] Equipe " + squad.getAbsoluteName() + "(Points de capture : " + squad.getAbsolutePoints() + ") (Total : " + squad.getPoints() + " :star:) " + position);
            messages.add("> __            Pseudo                     K    |     D    |     P__");
            for (final PlayerGame playerGame : squad.getPlayers()) {
                try {
                    StringBuilder playerName = new StringBuilder(playerGame.getName());
                    while (playerName.length() < 19) {
                        if (playerName.length() < 16)
                            playerName.append("\t");
                        playerName.append("\t");
                    }
                    String kills;
                    if (Math.floor(playerGame.getKills() / 10f) == 0) {
                        kills = "  " + playerGame.getKills() + "  ";
                    } else if (Math.floor(playerGame.getKills() / 10f) >= 10) {
                        kills = playerGame.getKills() + "  ";
                    } else {
                        kills = playerGame.getKills() + "";
                    }
                    String deaths;
                    if (Math.floor(playerGame.getDeaths() / 10f) == 0) {
                        deaths = "  " + playerGame.getDeaths() + "  ";
                    } else if (Math.floor(playerGame.getDeaths() / 10f) >= 10) {
                        deaths = playerGame.getDeaths() + "  ";
                    } else {
                        deaths = playerGame.getDeaths() + "";
                    }
                    String points;
                    if (Math.abs(Math.floor(playerGame.getPoints() / 10f)) == 0) {
                        points = "   " + playerGame.getPoints();
                    } else if (Math.abs(Math.floor(playerGame.getPoints() / 10f)) >= 10) {
                        points = "  " + playerGame.getPoints();
                    } else {
                        points = " " + playerGame.getPoints();
                    }
                    messages.add(">      ⇢ " + playerName + kills + "  |   " + deaths + "  |" + points + "");
                } catch(Exception ignored) {
                }
            }
            for (final PlayerGame playerGame : squad.getRemovedPlayers()) {
                try {
                    StringBuilder playerName = new StringBuilder(playerGame.getName()).append("~~");
                    while (playerName.length() < 21) {
                        if (playerName.length() < 16)
                            playerName.append(" ");
                        playerName.append(" ");
                    }
                    String kills;
                    if (Math.floor(playerGame.getKills() / 10f) == 0) {
                        kills = "  " + playerGame.getKills() + "  ";
                    } else if (Math.floor(playerGame.getKills() / 10f) >= 10) {
                        kills = playerGame.getKills() + "  ";
                    } else {
                        kills = playerGame.getKills() + "";
                    }
                    String deaths;
                    if (Math.floor(playerGame.getDeaths() / 10f) == 0) {
                        deaths = "  " + playerGame.getDeaths() + "  ";
                    } else if (Math.floor(playerGame.getDeaths() / 10f) >= 10) {
                        deaths = playerGame.getDeaths() + "  ";
                    } else {
                        deaths = playerGame.getDeaths() + "";
                    }
                    String points;
                    if (Math.abs(Math.floor(playerGame.getPoints() / 10f)) == 0) {
                        points = "    " + playerGame.getPoints();
                    } else if (Math.abs(Math.floor(playerGame.getPoints() / 10f)) >= 10) {
                        points = "  " + playerGame.getPoints();
                    } else {
                        points = "  " + playerGame.getPoints();
                    }
                    messages.add(">      ⇢ ~~" + playerName + kills + "  |   " + deaths + "   |" + points + "");
                } catch (Exception ignored) {
                }
            }
        }
        messages.add("~~===================================~~");
        sendMessages(channelId, messages);
    }

    private static void sendMessages(final String channelId, final List<String> messages) {
        final Player player = (Player) Bukkit.getServer().getOnlinePlayers().toArray()[0];
        try {
            final ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
            final DataOutputStream out = new DataOutputStream(byteArrayOut);
            out.writeUTF("sendDiscordMessage");
            out.writeUTF(channelId);
            for (final String string : messages) {
                out.writeUTF(string + System.lineSeparator());
            }
            player.sendPluginMessage(Main.getInstance(), "bungee:bot", byteArrayOut.toByteArray());
        } catch (IOException ignored) {
        }
    }

}
