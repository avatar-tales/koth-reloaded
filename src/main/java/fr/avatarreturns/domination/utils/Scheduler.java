package fr.avatarreturns.domination.utils;

import fr.avatarreturns.domination.game.Domination;

import java.util.Timer;
import java.util.TimerTask;

public class Scheduler {

    private final Timer timer;
    private final TimerTask timerTask;

    public Scheduler() {
        this.timer = new Timer();
        this.timerTask = new TimerTask() {
            @Override
            public void run() {
                Domination.getDominations().parallelStream().forEach(Domination::progress);
            }
        };
    }

    public void start() {
        this.timer.schedule(this.timerTask, 0L, 50L);
    }

    public void stop() {
        this.timer.cancel();
    }
}
