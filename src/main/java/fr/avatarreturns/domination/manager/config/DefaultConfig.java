package fr.avatarreturns.domination.manager.config;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class DefaultConfig {

    private static int timer = 3600;
    private static int teamNumber = 5;
    private static double yenPerPoints = 3;
    private static double reward = 100;
    private static int pointPerKill = 10;
    private static int pointPerDeath = 7;
    private static int secondsToCapture = 20;
    private static int spectateDuration = 20;
    private static double minPointsReward = 0;
    private static double minParticipationReward = 0;
    private static String channelId = "";

    public static void init(final String path) {
        final File file = new File(path, "config.default");
            try {
                if (!file.exists()) {
                    file.getParentFile().mkdirs();
                    file.createNewFile();
                }
                final YamlConfiguration yamlConfiguration = new YamlConfiguration();
                yamlConfiguration.load(file);
                for (final Field field : DefaultConfig.class.getDeclaredFields()) {
                    if (Modifier.isTransient(field.getModifiers()))
                        continue;
                    field.set(DefaultConfig.class, getOrAdd(yamlConfiguration, field.getName(), field.get(DefaultConfig.class)));
                }
                yamlConfiguration.save(file);
            } catch (Exception e) {
                e.printStackTrace();
            }

    }

    private static Object getOrAdd(final YamlConfiguration yamlConfiguration, final String path, final Object value) {
        if (yamlConfiguration.get(path) == null)
            yamlConfiguration.set(path, value);
        return yamlConfiguration.get(path);
    }

    public static int getTimer() {
        return timer;
    }

    public static int getTeamNumber() {
        return teamNumber;
    }

    public static double getYenPerPoints() {
        return yenPerPoints;
    }

    public static double getReward() {
        return reward;
    }

    public static int getPointPerKill() {
        return pointPerKill;
    }

    public static int getPointPerDeath() {
        return pointPerDeath;
    }

    public static int getSecondsToCapture() {
        return secondsToCapture;
    }

    public static String getChannelId() {
        return channelId;
    }

    public static int getSpectateDuration() {
        return spectateDuration;
    }

    public static double getMinPointsReward() {
        return minPointsReward;
    }

    public static double getMinParticipationReward() {
        return minParticipationReward;
    }
}
