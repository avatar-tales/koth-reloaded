package fr.avatarreturns.domination.manager;

import org.bukkit.plugin.java.JavaPlugin;

public abstract class IManager {

    public JavaPlugin javaPlugin;
    public String name;

    public IManager(final JavaPlugin javaPlugin, final String name) {
        this.javaPlugin = javaPlugin;
        this.name = name;
    }

    public abstract void init();

}
