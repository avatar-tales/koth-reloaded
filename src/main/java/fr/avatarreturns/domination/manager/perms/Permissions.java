package fr.avatarreturns.domination.manager.perms;

import org.bukkit.command.CommandSender;

public enum Permissions {

    ADMIN("admin", "domination.admin"),
    //DEBUG("debug", "domination.debug"),
    NONE("none", ""),
    CREATE("commands.create", "domination.commands.create"),
    DESTROY("commands.destroy", "domination.commands.destroy"),
    JOIN("commands.join", "domination.commands.join"),
    LEAVE("commands.leave", "domination.commands.leave"),
    LIST("commands.list", "domination.commands.list"),
    START("commands.start", "domination.commands.start"),
    EDIT_OTHER("commands.edit.other", "domination.commands.edit.other"),
    EDIT_TEAMS("commands.edit.teams", "domination.commands.edit.teams"),
    EDIT_PLATE("commands.edit.plate", "domination.commands.edit.plate"),
    EDIT_CIRCLE("commands.edit.circle", "domination.commands.edit.circle");

    private final String path;
    private String permission;

    Permissions(final String path, final String permission) {
        this.path = path;
        this.permission = permission;
    }

    public static boolean hasPermission(final CommandSender commandSender, final Permissions permission) {
        return hasPermission(commandSender, permission.get()) || hasPermission(commandSender, Permissions.ADMIN.get());
    }

    public static boolean hasPermission(final CommandSender commandSender, final String permission) {
        return commandSender.hasPermission(permission);
    }

    public String getPath() {
        return this.path;
    }

    public String get() {
        return this.permission;
    }

    public void set(final String permission) {
        this.permission = permission;
    }

    public boolean hasPermission(final CommandSender commandSender) {
        return hasPermission(commandSender, this);
    }

    @Override
    public String toString() {
        return this.permission;
    }

}
