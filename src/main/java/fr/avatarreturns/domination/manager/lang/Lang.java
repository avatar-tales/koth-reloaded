package fr.avatarreturns.domination.manager.lang;

import org.bukkit.ChatColor;

public enum Lang {

    PREFIX_INFO("prefix.info", "&3&lDomination &8&l➫&r"),
    PREFIX_DEBUG("prefix.debug", "&e&lDomination &6&l➫&r"),
    PREFIX_WARNING("prefix.warning", "&6&lDomination &c&l➫&r"),
    PREFIX_ERROR("prefix.error", "&c&lDomination &4&l➫&r"),


    INFO_CREATE("info.commands.create", "%prefix_info% &7Une instance a été créée avec le nom &6{0}&7."),
    INFO_DESTROY("info.commands.destroy", "%prefix_info% &7Instance détruite."),
    INFO_LIST("info.commands.list", "%prefix_info% &7Liste des Dominations : {0}"),
    INFO_EDIT_UPDATE("info.commands.edit.update", "%prefix_info% &7Instance mise à jour."),
    INFO_EDIT_CIRCLE_CREATE("info.commands.edit.circle.add", "%prefix_info% &7Cercle créé pour l'instance &6{0}&7."),
    INFO_EDIT_CIRCLE_REMOVE("info.commands.edit.circle.remove", "%prefix_info% &7Cercle supprimé pour l'instance &6{0}&7."),
    INFO_EDIT_CIRCLE_UPDATE("info.commands.edit.circle.edit", "%prefix_info% &7Cercle mis à jour."),
    INFO_EDIT_PLATE_ADD("info.commands.edit.plate.add", "%prefix_info% &7Plaque ajoutée pour l'instance &6{0}."),
    INFO_EDIT_PLATE_REMOVE("info.commands.edit.plate.remove", "%prefix_info% &7Plaque supprimée pour l'instance &6{0}."),
    INFO_ENTER_TEAM("info.game.enter.team", "%prefix_info% &7Vous êtes dans la team &6{0}&7. &7Vos coéquipiers sont {1}."),
    INFO_STARTER("info.game.start", "%prefix_info% &7La partie vient de démarrer."),
    INFO_START("info.game.start", "%prefix_info% &7Que la partie commence !"),
    INFO_DEATH("info.game.death", "%prefix_info% &7Vous êtes mort. Vous perdez &6{0} points&7. Vous réapparaîtrez dans &6{1} secondes&7."),
    INFO_KILL("info.game.kill", "%prefix_info% &7Vous avez fait un kill. Vous remportez &6{0} points&7."),
    INFO_JOIN_RETRIEVE("info.game.join.retrieve", "%prefix_info% &6{0} &7a rejoint la partie en cours. Ses données ont été restorées."),
    INFO_JOIN_IN_GAME("info.game.join.in-game", "%prefix_info% &6{0} &7a rejoint la partie en cours. Il est dans l'escouade &6{1}."),
    INFO_JOIN("info.game.join.normal", "%prefix_info% &6{0} &7a rejoint la partie de Domination."),
    INFO_LEAVE("info.game.leave", "%prefix_info% &6{0} &7a quitté la partie de Domination."),
    INFO_GET_MONEY("info.game.win.get", "%prefix_info% &7Vous venez de gagner &6{0} Y&7."),
    INFO_GET_PARTMONEY("info.game.paricipate", "%prefix_info% &7Vous venez de gagner &6{0} Y &7pour votre participation."),
    INFO_WINNER("info.game.win.all", "%prefix_info% &7Victoire de ou des escouades &6{0}&7."),
    INFO_TAKE_PLATE("info.game.plate", "%prefix_info% &7L'équipe &6{0} &7vient de capturer la plaque &6{1}&7."),
    INFO_TIMER("info.game.timer", "%prefix_info% &7Il reste {0} secondes !"),


    WARNING_TEAMDAMAGE("warning.game.teamdamage", "%prefix_warning% &7Vous venez de toucher votre coéquipier &6{0} &7!"),
    WARNING_TEAMKILL_VICTIM("warning.game.teamkill.victim", "%prefix_warning% &7Votre coéquipier &6{0} &7vous a tué !"),
    WARNING_TEAMKILL_ATTACKER("warning.game.teamkill.attacker", "%prefix_warning% &7Vous avez tué votre coéquipier &6{0}&7. Vous ne gagnez aucun point."),
    WARNING_FALLBACKPLAYER("warning.game.fallbackplayer", "%prefix_warning% &7Vous vous êtes déconnecté durant une partie de Domination qui a été remporté par &6{0}&7. Vous venez d'être téléporté au point de retour de la Domination."),
    WARNING_SQUAD_DOMINATE("info.game.dominate", "%prefix_info% &7L'équipe &6{0} &7domine le jeu ! ({1} plaques capturées)"),


    ERROR_PERMISSION("error.permission", "%prefix_error% &cVous n'avez pas la permission de faire cela"),
    ERROR_USAGE("error.usage", "%prefix_error% &cUsage: &4/{0}&c."),
    ERROR_EXIST("error.exist", "%prefix_error% &cUne instance portant ce nom existe déjà."),
    ERROR_START("error.start", "%prefix_error% &cLa partie est déjà lancée."),
    ERROR_NOT_ENOUGH("error.enough", "%prefix_error% &cIl n'y a pas assez de joueur pour lancer la partie &4{0}&c."),
    ERROR_JOIN_CANT("error.join.cant", "%prefix_error% &cVous ne pouvez pas rejoindre cette partie."),
    ERROR_IN_GAME("error.in-game", "%prefix_error% &cVous êtes déjà en partie."),
    ERROR_NOT_IN_GAME("error.not-in-game", "%prefix_error% &cVous n'êtes pas en partie."),
    ERROR_EDIT_CIRCLE_FOUND("error.commands.edit.circle.found", "%prefix_error% &cUn cercle avec ce nom a déjà été défini pour cette instance."),
    ERROR_EDIT_CIRCLE_NOT_FOUND("error.commands.edit.circle.notfound", "%prefix_error% &cAucun cercle avec ce nom n'a été trouvé."),
    ERROR_EDIT_PLATE_ADD("error.commands.edit.circle.add", "%prefix_error% &cUne plaque utilise déjà cette région worldguard."),
    ERROR_EDIT_PLATE_REGION_NOTFOUND("error.commands.edit.plate.region.notfound", "%prefix_error% &cAucune région worldguard trouvée."),
    ERROR_EDIT_PLATE_NOTFOUND("error.commands.edit.plate.notfound", "%prefix_error% &cAucune plaque trouvée."),
    ERROR_NOT_PLAYER("error.not-player", "%prefix_error% &cSeul un joueur peut exécuter cette commande."),
    ERROR_INEXIST("error.inexist", "%prefix_error% &cAucune instance ne porte ce nom."),
    ERROR_EMPTY_LIST("error.commands.list", "%prefix_error% &cIl n'y a aucune instance.");

    private final String path;
    private String message;

    Lang(final String path, final String message) {
        this.path = path;
        this.message = message;
    }

    public String getPath() {
        return this.path;
    }

    public String get() {
        return ChatColor.translateAlternateColorCodes('&', this.message
                .replace("%prefix_info%", Lang.PREFIX_INFO.toString())
                .replace("%prefix_debug%", Lang.PREFIX_DEBUG.toString())
                .replace("%prefix_warning%", Lang.PREFIX_WARNING.toString())
                .replace("%prefix_error%", Lang.PREFIX_ERROR.toString()));
    }

    public String get(String... args) {
        String message = this.get();
        for (int i = 0; i < args.length; i++) {
            message = message.replace("{" + i + "}", args[i]);
        }
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public void set(final String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return this.message;
    }
}
